package ch.gerberrenato.rezepte.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * N�tzliche Funktionen zur Datenbank
 * @author Renato Gerber
 *
 */
public final class DbUtil {
	
	private static String DbConnString = "jdbc/RezepteDSPool";
	
	public DbUtil() {
	}

	/**
	 * Diese Methode liefert eine neue Verbindung zur Datenbank zur�ck.
	 * 
	 * @param dataSourceName
	 *            Name der Datasource
	 * @return eine neue Datenbankverbindung
	 * @throws SQLException
	 *             SQL Exception
	 * @throws NamingException
	 *             Naming Exception
	 */
	public static Connection open()
			throws SQLException, NamingException {
		
		InitialContext context = new InitialContext();
		DataSource ds = (DataSource) context.lookup(DbConnString);
		return ds.getConnection();
	}

	/**
	 * Schliesst alle Datenbank Resourcen
	 * 
	 * @param con
	 *            zu schliessende Datenbank Connection
	 * @param stmt
	 *            zu schliessendes SQL Statement
	 * @param rs
	 *            zu schliessendes Result Set
	 */
	public static void close(Connection con, PreparedStatement pstmt, Statement stmt, ResultSet rs) {

		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException se1) {
				se1.printStackTrace();
			}
		}
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException se2) {
				se2.printStackTrace();
			}
		}
		if (pstmt != null) {
			try {
				pstmt.close();
			} catch (SQLException se2) {
				se2.printStackTrace();
			}
		}
		if (con != null) {
			try {
				con.close();
			} catch (SQLException se3) {
				se3.printStackTrace();
			}
		}
	}
}
