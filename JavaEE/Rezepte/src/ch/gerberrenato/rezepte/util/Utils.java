package ch.gerberrenato.rezepte.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * N�tzliche Methodne die immer wieder verwendet werden in allen Packeten
 * @author Renato Gerber
 *
 */
public class Utils {
	
	/**
	 * MEthode gibt das Maximle Datum aus.
	 * @return Date
	 */
	public static Date getEndDate(){
		
		SimpleDateFormat simpleDate = new SimpleDateFormat("dd/MM/yyyy");
		Date endDate = null;
		
		try {
			endDate = simpleDate.parse("31/12/2099");
			
		} catch (ParseException e1) {
			System.out.print(e1.toString());
			System.out.print(e1.getStackTrace());
		}
		
		return endDate;
	}
	
}
