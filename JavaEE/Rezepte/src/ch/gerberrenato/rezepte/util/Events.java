package ch.gerberrenato.rezepte.util;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.inject.Qualifier;

/**
 * Diese Klasse erzeut die Events f�r die ADD, UPDATE und DELTE Methoden bereit. 
 * @author Renato Gerber
 *
 */
public class Events {
	
	@Qualifier
	@Target({ FIELD, PARAMETER })
	@Retention(RUNTIME)
	public @interface Added {
	}
	
	@Qualifier
	@Target({ FIELD, PARAMETER })
	@Retention(RUNTIME)
	public @interface Deleted {
	}
	
	@Qualifier
	@Target({ FIELD, PARAMETER })
	@Retention(RUNTIME)
	public @interface Updated {
	}
}
