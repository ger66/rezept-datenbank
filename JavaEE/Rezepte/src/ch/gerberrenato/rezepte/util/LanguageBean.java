package ch.gerberrenato.rezepte.util;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;


/**
 * Klasse f�r die Sprachen und die Auswahl der Sprachen, die Textdatein sind mit den Textbl�cken sind nicht hier gespeichert.
 * @author Renato Gerber
 *
 */
@ManagedBean(name = "language", eager = true)
@SessionScoped
public class LanguageBean implements Serializable {

   private static final long serialVersionUID = 1L;
   private String locale = "de";

   private static Map<String,Object> countries;
   
   static{
      countries = new LinkedHashMap<String,Object>();
      countries.put("English", Locale.ENGLISH);
      countries.put("Deutsch", Locale.GERMAN);
   }

   public Map<String, Object> getCountries() {
      return countries;
   }

   public String getLocale() {
      return locale;
   }

   public void setLocale(String locale) {
      this.locale = locale;
   }

   //value change event listener
   public void localeChanged(ValueChangeEvent e){

      String newLocaleValue = e.getNewValue().toString();

      for (Map.Entry<String, Object> entry : countries.entrySet()) {

    	  if(entry.getValue().toString().equals(newLocaleValue)){
            FacesContext.getCurrentInstance().getViewRoot().setLocale((Locale)entry.getValue());
         }
      }
   }
}