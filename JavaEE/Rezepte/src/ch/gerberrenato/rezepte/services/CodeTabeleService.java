package ch.gerberrenato.rezepte.services;

import java.util.List;

import ch.gerberrenato.rezepte.model.CodeTabelle;

public interface CodeTabeleService {
	List<CodeTabelle> getAllCodes();
	List<CodeTabelle> getAllChildCodes();
	CodeTabelle add(CodeTabelle codeElement);
	CodeTabelle update(CodeTabelle codeTabelle);
	void delete(CodeTabelle codeTabelle);
	void delete(Long codeTabellenNr);
	List<CodeTabelle> getGruppe(String gruppe);
}
