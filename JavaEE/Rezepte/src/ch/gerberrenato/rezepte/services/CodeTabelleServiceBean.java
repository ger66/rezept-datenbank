package ch.gerberrenato.rezepte.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.naming.NamingException;

import ch.gerberrenato.rezepte.model.CodeTabelle;
import ch.gerberrenato.rezepte.util.DbUtil;
/**
 * Klasse die Daten von der Datenbank holt f�r die Codetabellen Tabelle.
 * Die SQL's sind zum Tiel als private String implementiert.
 * @author Renato Gerber
 *
 */
@Stateless
public class CodeTabelleServiceBean implements CodeTabeleService {
	
	private String sqlAllCodes = "SELECT CodetabelleNr, Bezeichnung, KurzBezeichnung, ownerNr, aktiv_f, isParent, 0 as Codetabellentree_Id FROM rezeptedatenbank.codetabelle;";
	private String sqlAllChildCodes = "SELECT CodetabelleNr, Bezeichnung, KurzBezeichnung, ownerNr, aktiv_f, isParent, 0 as Codetabellentree_Id FROM rezeptedatenbank.codetabelle Where isParent <> 1;";
	private String sqlAllChildCodesForParent = "SELECT code.CodetabelleNr, code.Bezeichnung, code.KurzBezeichnung, code.ownerNr, code.aktiv_f, code.IsParent, cjt.Codetabellentree_Id FROM rezeptedatenbank.codetabelle code INNER JOIN rezeptedatenbank.codetabelletree cjt ON cjt.child = code.CodetabelleNr AND cjt.parent = ? WHERE 1=1 and code.aktiv_f = 1  and code.IsParent = 2;";
	private String sqlProduktKategorie = "SELECT * FROM produktkategorie";
	private String sqlMenueKat = "SELECT * FROM menuekategorien";
	private String sqlEinheiten = "SELECT * FROM einheiten";
	private String sqlTcmElemente = "SELECT * FROM tcmelemente";
	
	@Resource
	private SessionContext sessionContext;
	
	/**
	 * Liest die Daten von der Datenbank mit einem SQL als Parameter
	 * @param String sql
	 * @return List<CodeTabelle>
	 */
	private List<CodeTabelle> getCodesFromDB(String sql) {
		
		List<CodeTabelle> codeTabeleListe = new ArrayList<>();
		Statement stmt = null;
		ResultSet rs = null;
		Connection con = null;
		
		try {
			con = DbUtil.open();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			codeTabeleListe = resultsetToList(rs);
			
			// Child Codes anh�ngen
			for (CodeTabelle temp : codeTabeleListe) {
				temp.setChildCodes( getChilds(temp.getCodeTabelleNr()) );
			}

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(con, null, stmt, rs);
		}
		
		return codeTabeleListe;
	}
	
	/**
	 * Bereitet das ResultSet auf in einer Liste
	 * @param ResultSet rsLocal
	 * @return List<CodeTabelle> 
	 * @throws SQLException
	 */
	private List<CodeTabelle> resultsetToList(ResultSet rsLocal) throws SQLException{
		
		List<CodeTabelle> resList = new ArrayList<CodeTabelle>();
		
		while (rsLocal.next()) {
			// Retrieve by column name
			long id = rsLocal.getLong("CodetabelleNr");
			String bezeichnung = rsLocal.getString("Bezeichnung");
			String kurzBezeichnung = rsLocal.getString("KurzBezeichnung");
			int ownerNr = rsLocal.getInt("ownerNr");
			int aktiv_f = rsLocal.getInt("aktiv_f");
			int isParent = rsLocal.getInt("isParent");
			long joinId = rsLocal.getLong("Codetabellentree_Id");
		
			CodeTabelle codeElement = new CodeTabelle(id, bezeichnung, kurzBezeichnung, aktiv_f, ownerNr, isParent, null, joinId);
			resList.add(codeElement);
		}
		return resList;
	}
	
	/**
	 * Git die Kinder der ParentId (CodeTabellen ID) zur�ck
	 * @param long parentId
	 * @return List<CodeTabelle>
	 */
	private List<CodeTabelle> getChilds(long parentId) {
		
		List<CodeTabelle> resList = new ArrayList<CodeTabelle>();
		PreparedStatement pstmtLocal = null;
		Connection con = null;
		
		try {
			con = DbUtil.open();
			pstmtLocal = con.prepareStatement(sqlAllChildCodesForParent);
			pstmtLocal.setLong(1, parentId);
			resList = resultsetToList( pstmtLocal.executeQuery());
			
		} catch (SQLException | NamingException e){
			System.out.print(e.toString());
		} finally {
			DbUtil.close(con, pstmtLocal, null, null);
		}

		return resList;
	}
	
	/**
	 * Code-Element hinzuf�gen
	 * @param CodeTabelle codeElement
	 *  @return CodeTabelle
	 */
	public CodeTabelle add(CodeTabelle codeElement) {
		PreparedStatement pstmt = null;

		// Default Value setzten
		codeElement.setOwnerNr(1);
		Connection con = null;
		
		try {
			con = DbUtil.open();
			String sql = "INSERT INTO rezeptedatenbank.codetabelle (Bezeichnung, KurzBezeichnung, ownerNr, aktiv_f, isParent) VALUES (?, ?, ?, ?, ?);";
			pstmt = con.prepareStatement(sql);
			
			pstmt.setString(1, codeElement.getBezeichnung() );
			pstmt.setString(2, codeElement.getKurzBezeichnung());
			pstmt.setInt(3, codeElement.getOwnerNr() );
			pstmt.setInt(4, codeElement.getAktiv_f() );
			pstmt.setInt(5, codeElement.getIsParent() );

			pstmt.executeUpdate();

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(con, pstmt, null, null);
		}
		
		return codeElement;
	}
	
	/**
	 * L�schen eines Elements
	 * @param CodeTabelle codeElement
	 */
	public void delete(CodeTabelle codeTabelle) {
		delete(codeTabelle.getCodeTabelleNr());
	}
	
	/**
	 * L�schen eines Elements
	 * @param long codeElementNr
	 */
	public void delete(Long codeElementNr) {

		PreparedStatement pstmt = null;
		Connection con = null;
		
		try {
			con = DbUtil.open();
			String sql = "DELETE FROM rezeptedatenbank.codetabelle WHERE CodetabelleNr = ?";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, codeElementNr);
			pstmt.executeUpdate();

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(con, pstmt, null, null);
		}
		
	}
	
	/**
	 * Aktualiseren eines Code-Elements
	 * @param CodeTabelle codeElement
	 * @return CodeTabelle
	 */
	public CodeTabelle update(CodeTabelle codeElement) {

		PreparedStatement pstmt = null;
		Connection con = null;
		
		try {
			con = DbUtil.open();
			String sql = "UPDATE rezeptedatenbank.codetabelle SET Bezeichnung = ?, KurzBezeichnung = ?, ownerNr = ?, aktiv_f = ?, isParent = ? WHERE CodetabelleNr = ?;";
			
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, codeElement.getBezeichnung());
			pstmt.setString(2, codeElement.getKurzBezeichnung());
			pstmt.setInt(3, codeElement.getOwnerNr());
			pstmt.setInt(4, codeElement.getAktiv_f());
			pstmt.setInt(5, codeElement.getIsParent() );
			pstmt.setLong(6, codeElement.getCodeTabelleNr());
			pstmt.executeUpdate();

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(con, pstmt, null, null);
		}

		return codeElement;
	}
	
	/**
	 * Gibt alle Codes aus
	 * @return List<CodeTabelle>
	 */
	@Override
	public List<CodeTabelle> getAllCodes() {
		return getCodesFromDB(sqlAllCodes);
	}
	
	/**
	 * Gibt alle ChildCodes aus
	 * @return List<CodeTabelle>
	 */
	@Override
	public List<CodeTabelle> getAllChildCodes() {
		return getCodesFromDB(sqlAllChildCodes);
	}
	
	/**
	 * Gibt alle Werte einer Gruppe zur�ck
	 * @param String gruppe
	 * @return List<CodeTabelle>
	 */
	@Override
	public List<CodeTabelle> getGruppe(String gruppe) {
		
		Statement stmt = null;
		ResultSet rs = null;
		List<CodeTabelle> resList = new ArrayList<>();
		Connection con = null;
		
		try {
			String sql = "";
			// Der Gruppen Parameter muss noch evaluiert werden, denn der bestimt das SQL.
			switch (gruppe) {
			case "PRODKAT":
				sql = sqlProduktKategorie;
				break;
			case "EINHEITEN":
				sql = sqlEinheiten;
				break;
			case "MENUEKAT":
				sql = sqlMenueKat;
				break;
			case "TCM":
				sql = sqlTcmElemente;
				break;
			default:
				sql = "";
				break;
			}
			
			con = DbUtil.open();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			resList = resultsetToList(rs);

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(con, null, stmt, rs);
		}
		return resList;
	}

}

