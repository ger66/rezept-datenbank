package ch.gerberrenato.rezepte.services;

import java.util.List;

import ch.gerberrenato.rezepte.model.Produkt;

public interface ProdukteService {
	List<Produkt> getAllProdukts();
	Produkt add(Produkt element);
	Produkt update(Produkt element);
	void delete(Produkt element);
	void delete(Long produktNr);
}
