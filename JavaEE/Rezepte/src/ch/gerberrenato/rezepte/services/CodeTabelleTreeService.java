package ch.gerberrenato.rezepte.services;

import java.util.List;

import ch.gerberrenato.rezepte.model.CodeTabelleTree;

public interface CodeTabelleTreeService {
	List<CodeTabelleTree> getAllCodeTree();
	List<CodeTabelleTree> getAllCodeTree(long codeTabelleTreeId);
	List<CodeTabelleTree> getAllCodesFromId(long searchId);
	CodeTabelleTree add(CodeTabelleTree codeElement);
	CodeTabelleTree update(CodeTabelleTree codeTabelle);
	void delete(CodeTabelleTree codeTabelle);
	void delete(Long codeTabellenTreeNr);
}
