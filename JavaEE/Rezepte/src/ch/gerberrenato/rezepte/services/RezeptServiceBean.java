package ch.gerberrenato.rezepte.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.naming.NamingException;

import ch.gerberrenato.rezepte.model.Rezept;
import ch.gerberrenato.rezepte.util.DbUtil;
import ch.gerberrenato.rezepte.util.Utils;
/**
 * Alle Methoden die die Daten zum Rezept von de rDatenbank laden.
 * @author Renato Gerber
 *
 */
@Stateless
public class RezeptServiceBean implements RezeptService {

	@Resource
	private SessionContext sessionContext;
	
	// SQL Querys die sehr gross sind, werden in den einzelnen Methoden referenziert.
	private String sqlRezepte = "SELECT rez.RezeptNr, rez.Name, rez.Beschreibung, rez.ownerNr, rez.GueltigAb, rez.GueltigBis, rez.kalorien, CONCAT(rez.kalorien, ' kcal') as kalorienBez, rez.kategorieNr, rez.aktiv_f, code.Bezeichnung as Kategorie "
							  + "FROM rezeptedatenbank.rezepte rez LEFT OUTER JOIN codetabelle code on code.CodetabelleNr = rez.kategorieNr";
	private String sqlRezepteSucheEinfach = "SELECT rez.RezeptNr, rez.Name, rez.Beschreibung, rez.ownerNr, rez.GueltigAb, rez.GueltigBis, rez.kalorien, CONCAT(rez.kalorien, ' kcal') as kalorienBez, rez.kategorieNr, rez.aktiv_f, code.Bezeichnung as Kategorie "
			  				  + "FROM rezeptedatenbank.rezepte rez LEFT OUTER JOIN codetabelle code on code.CodetabelleNr = rez.kategorieNr "
			  				  + "WHERE lower( rez.Name ) like ? OR lower( rez.Beschreibung ) like ? OR lower( code.Bezeichnung ) like ?";
	private String sqlRezepteSucheProdukte = "SELECT rez.RezeptNr, rez.Name, rez.Beschreibung, rez.ownerNr, rez.GueltigAb, rez.GueltigBis, rez.kalorien, CONCAT(rez.kalorien, ' kcal') as kalorienBez, rez.kategorieNr, rez.aktiv_f,"
							  + " code.Bezeichnung as Kategorie, rezeptNummern.AnzahlProdukte FROM rezeptedatenbank.rezepte rez "
							  + " LEFT OUTER JOIN codetabelle code on code.CodetabelleNr = rez.kategorieNr "
							  + " INNER JOIN (SELECT zut.rezeptNr, count(*) AS AnzahlProdukte FROM rezeptedatenbank.produkte prod "
							  + " INNER JOIN zutaten zut ON zut.produktNr = prod.ProdukteNr WHERE prod.ProdukteNr in (?,?,?,?) "
							  + " GROUP BY zut.rezeptNr) AS rezeptNummern ON rezeptNummern.rezeptNr = rez.rezeptNr AND rezeptNummern.AnzahlProdukte > 0 "
							  + " ORDER BY AnzahlProdukte desc, rez.Name";
	
	/**
	 * Git alle Rezepte zur�ck
	 * @return List<Rezept>
	 */
	public List<Rezept> getAllRezepte() {

		Statement stmt = null;
		ResultSet rs = null;
		List<Rezept> rezepteListe = new ArrayList<>();
		
		Connection con = null;
		
		try {
			con = DbUtil.open();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sqlRezepte);
			rezepteListe = getListFormRezepte(rs);
			
		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(con, null, stmt, rs);
		}
		return rezepteListe;
	}
	
	/**
	 * Erstellt aus dem ResultSet eine Liste
	 * @param ResultSet rs
	 * @return List<Rezept>
	 * @throws SQLException
	 */
	private List<Rezept> getListFormRezepte(ResultSet rs) throws SQLException{
		
		List<Rezept> resList = new ArrayList<>();
		
		while (rs.next()) {
			// Retrieve by column name
			long id = rs.getLong("RezeptNr");
			String name = rs.getString("Name");
			String beschreibung = rs.getString("Beschreibung");
			int owner = rs.getInt("ownerNr");
			java.util.Date gueltigAb = rs.getDate("GueltigAb");
			java.util.Date gueltigBis = rs.getDate("GueltigBis");
			int kalorien = rs.getInt("kalorien");
			int kategorieNr = rs.getInt("kategorieNr");
			int aktiv = rs.getInt("aktiv_f");
			String kategorieBez = rs.getString("Kategorie");
			String kalorienBez = rs.getString("kalorienBez");

			Rezept rezept = new Rezept(id, name, beschreibung, kalorien, kategorieNr, aktiv, owner, gueltigAb, gueltigBis);
			rezept.setKategorieBez(kategorieBez);
			rezept.setKalorienBez(kalorienBez);
			resList.add(rezept);
		}
		
		return resList;
	}
	
	/**
	 * F�gt ein Rezept in die Datenbank ein.
	 * @param Rezept rezept
	 * @return Rezept
	 */
	public Rezept add(Rezept rezept) {
		
		PreparedStatement pstmt = null;
		Date now = new Date();
		long endTime = Utils.getEndDate().getTime();
		// Default Owner setzten
		rezept.setOwnerNr(1);
		
		Connection con = null;
		
		try {
			con = DbUtil.open();
			
			String sql = "INSERT INTO rezeptedatenbank.rezepte (Name, Beschreibung, ownerNr, GueltigAb, GueltigBis, kalorien, kategorieNr, aktiv_f) VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
			pstmt = con.prepareStatement(sql);
						
			pstmt.setString(1, rezept.getName());
			pstmt.setString(2, rezept.getBeschreibung());
			pstmt.setInt(3, rezept.getOwnerNr() );
			pstmt.setDate(4, new java.sql.Date( now.getTime() ));
			pstmt.setDate(5, new java.sql.Date( endTime ));
			pstmt.setInt(6, rezept.getKalorien() );
			pstmt.setInt(7, rezept.getKategorieNr());
			pstmt.setInt(8, rezept.getAktiv_f());

			pstmt.executeUpdate();

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(con, pstmt, null, null);
		}
		
		return rezept;
	}
	
	/**
	 * L�scht ein Rezept auf der Datenbank
	 * @param long RezeptNr
	 */
	public void delete(Long rezeptNr) {
		
		PreparedStatement pstmt = null;
		Connection con = null;
		
		try {
			con = DbUtil.open();
			
			String sql = "DELETE FROM rezeptedatenbank.rezepte WHERE RezeptNr = ?";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, rezeptNr);
			pstmt.executeUpdate();

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(con, pstmt, null, null);
		}
	}
	
	/**
	 * L�scht ein Rezept eauf der Datenbank.
	 * @param Rezept rezept
	 */
	public void delete(Rezept rezept) {
		delete(rezept.getRezeptNr());
	}

	/**
	 * Aktualisert ein Rezept auf der Datenbank.
	 * @param Rezept rezept
	 * @return Rezept
	 */
	public Rezept update(Rezept rezept) {
		
		PreparedStatement pstmt = null;
		Connection con = null;
		
		try {
			con = DbUtil.open();
			
			String sql = "UPDATE rezeptedatenbank.rezepte SET Name=?, Beschreibung=?, ownerNr=?, GueltigAb=?, GueltigBis=?,  kalorien=?,  kategorieNr=?, aktiv_f=? WHERE RezeptNr = ?;";
			
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, rezept.getName());
			pstmt.setString(2, rezept.getBeschreibung());
			pstmt.setInt(3, rezept.getOwnerNr());
			pstmt.setDate(4, new java.sql.Date(rezept.getGueltigAb().getTime()));
			pstmt.setDate(5, new java.sql.Date(rezept.getGueltigBis().getTime()));
			pstmt.setInt(6, rezept.getKalorien());
			pstmt.setInt(7, rezept.getKategorieNr());
			pstmt.setInt(8, rezept.getAktiv_f() );
			pstmt.setLong(9, rezept.getRezeptNr());
			
			pstmt.executeUpdate();

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(con, pstmt, null, null);
		}
		
		return rezept;
	}
	
	/**
	 * Git die Rezepte mit dem Suchtext aus.
	 * @param String suchText
	 * @return List<Rezept>
	 */
	@Override
	public List<Rezept> getRezepteSearch(String suchText) {
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<Rezept> rezepteListe = new ArrayList<>();
		Connection con = null;
		
		try {
			if (suchText.length() > 0) {
				String param = "%" + suchText.trim().toLowerCase() + "%";
				
				con = DbUtil.open();
				pstmt = con.prepareStatement(sqlRezepteSucheEinfach);
				pstmt.setString(1, param);
				pstmt.setString(2, param);
				pstmt.setString(3, param);
				rs = pstmt.executeQuery();
				rezepteListe = getListFormRezepte(rs);
			} else {
				rezepteListe = getAllRezepte();
			}
			
		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(con, pstmt, null, rs);
		}
		return rezepteListe;
	}
	
	/**
	 * Gibt die Rezept die der Suche entsprechen zur�ck
	 * @param long produkt1
	 * @param long produkt2
	 * @param long produkt3
	 * @param long produkt4
	 * @return List<Rezept>
	 */
	@Override
	public List<Rezept> getRezepteSearch(long produkt1, long produkt2, long produkt3, long produkt4) {

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<Rezept> rezepteListe = new ArrayList<>();
		Connection con = null;
		
		try {
			if ((produkt1 + produkt2 + produkt3 + produkt4) > 0) {
				
				con = DbUtil.open();
				pstmt = con.prepareStatement(sqlRezepteSucheProdukte);
				pstmt.setLong(1, produkt1);
				pstmt.setLong(2, produkt2);
				pstmt.setLong(3, produkt3);
				pstmt.setLong(4, produkt4);
				rs = pstmt.executeQuery();
				rezepteListe = getListFormRezepte(rs);

			} else {
				rezepteListe = getAllRezepte();
			}
			
		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(con, pstmt, null, rs);
		}
		return rezepteListe;
	}
}
