package ch.gerberrenato.rezepte.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.naming.NamingException;

import ch.gerberrenato.rezepte.model.CodeTabelleTree;
import ch.gerberrenato.rezepte.util.DbUtil;
/**
 * Alle Methodne die Daten von der Datenbank laden von der Tabelle CodetabellenTree
 * @author Renato Gerber
 *
 */
@Stateless
public class CodeTabelleTreeServiceBean implements CodeTabelleTreeService {
	
	@Resource
	private SessionContext sessionContext;
	private String sqlAllCodesWithParentId = "SELECT Codetabellentree_Id, Parent, Child FROM rezeptedatenbank.codetabelletree WHERE parent = ?;";
	
	/**
	 * Gibt alle Kind-Eltenr (Parent-Child) Beziehungen aus
	 * @return List<CodeTabelleTree> 
	 */
	public List<CodeTabelleTree> getAllCodeTree() {

		List<CodeTabelleTree> resultList = new ArrayList<CodeTabelleTree>();
		ResultSet rsLocal = null;
		Statement stmt = null;
		Connection con = null;
		
		try {
			con = DbUtil.open();
			String sql = "SELECT Codetabellentree_Id, Parent, Child FROM rezeptedatenbank.codetabelletree";
			stmt = con.createStatement();
			rsLocal = stmt.executeQuery(sql);
			
			while (rsLocal.next()) {
				// Retrieve by column name
				long id = rsLocal.getLong("Codetabellentree_Id");
				long parent = rsLocal.getLong("Parent");
				long child = rsLocal.getLong("child");
				
				CodeTabelleTree codeTreeElement = new CodeTabelleTree(id, parent, child);
				resultList.add(codeTreeElement);
			}
		} catch (SQLException | NamingException e){
			System.out.print(e.toString());
		} finally {
			DbUtil.close(con, null, stmt, rsLocal);
		}
		
		return resultList;
	}
	
	/**
	 * Gibt alle Kind-Eltern Beziehungen aus, die zu einem Elemnt geh�ren
	 * @param long codeTabelleTreeId
	 * @return List<CodeTabelleTree>
	 */
	@Override
	public List<CodeTabelleTree> getAllCodeTree(long codeTabelleTreeId) {
		List<CodeTabelleTree> resultList = new ArrayList<CodeTabelleTree>();
		ResultSet rsLocal = null;
		Statement stmt = null;
		Connection con = null;
		
		try {
			con = DbUtil.open();
			String sql = "SELECT Codetabellentree_Id, Parent, Child FROM rezeptedatenbank.codetabelletree WHERE Parent = 1";
			stmt = con.createStatement();
			rsLocal = stmt.executeQuery(sql);
			
			while (rsLocal.next()) {
				// Retrieve by column name
				long id = rsLocal.getLong("Codetabellentree_Id");
				long parent = rsLocal.getLong("Parent");
				long child = rsLocal.getLong("child");
				
				CodeTabelleTree codeTreeElement = new CodeTabelleTree(id, parent, child);
				resultList.add(codeTreeElement);
			}
		} catch (SQLException | NamingException e){
			System.out.print(e.toString());
		} finally {
			DbUtil.close(con, null, stmt, rsLocal);
		}
		
		return resultList;
	}
	
	/**
	 * Gibt alle Beziehungen aus die zu einer such ID vorhanden sind
	 * @param long searchId
	 * @return List<CodeTabelleTree>
	 */
	@Override
	public List<CodeTabelleTree> getAllCodesFromId(long searchId) {
		
		List<CodeTabelleTree> list = new ArrayList<CodeTabelleTree>();
		
		PreparedStatement pstmt = null;
		Connection con = null;
		
		try {
			con = DbUtil.open();
			
			pstmt = con.prepareStatement(sqlAllCodesWithParentId);
			list = getDataFromDbPrepStmt(pstmt, con);
		
		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(con, pstmt, null, null);
		}
		return list;
	}
	
	/**
	 * List die Daten mit einem PreparedStatement von der Datenbank ein.
	 * @param PreparetStatment pstmtLocal
	 * @param Connection conLocal
	 * @return List<CodeTabelleTree>
	 */
	private List<CodeTabelleTree> getDataFromDbPrepStmt(PreparedStatement pstmtLocal, Connection conLocal) {
		
		List<CodeTabelleTree> resultList = new ArrayList<CodeTabelleTree>();
		ResultSet rsLocal = null;
		Connection con = null;
		
		try {
			con = DbUtil.open();
			rsLocal = pstmtLocal.executeQuery();
			
			while (rsLocal.next()) {
				// Retrieve by column name
				long id = rsLocal.getLong("Codetabellentree_Id");
				long parent = rsLocal.getLong("Parent");
				long child = rsLocal.getLong("child");
				
				CodeTabelleTree codeTreeElement = new CodeTabelleTree(id, parent, child);
				resultList.add(codeTreeElement);
			}
		} catch (SQLException | NamingException e){
			System.out.print(e.toString());
		} finally {
			DbUtil.close(con, pstmtLocal, null, rsLocal);
		}

		return resultList;
	}
	/**
	 * Hizuf�gen einer Beziehung Kind-Eltern (Parent-Child) in die Datenbank
	 * @param CodeTabelleTree codeTreeElement
	 * @return CodeTabelleTree
	 */
	@Override
	public CodeTabelleTree add(CodeTabelleTree codeTreeElement) {
		
		PreparedStatement pstmt = null;
		Connection con = null;
		
		try {
			con = DbUtil.open();
			
			String sql = "INSERT INTO rezeptedatenbank.codetabelletree (parent, child) VALUES (?, ?);";
			pstmt = con.prepareStatement(sql);
			
			pstmt.setLong(1, codeTreeElement.getParent() );
			pstmt.setLong(2, codeTreeElement.getChild() );
			
			pstmt.executeUpdate();

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(null, pstmt, null, null);
		}
		
		return codeTreeElement;
	}
	
	/**
	 * Aktualiseren einer Beziehung Kind-Eltern (Parent-Child) in die Datenbank
	 * @param CodeTabelleTree codeTreeElement
	 * @return CodeTabelleTree 
	 */
	@Override
	public CodeTabelleTree update(CodeTabelleTree codeTreeElement) {

		PreparedStatement pstmt = null;
		Connection con = null;
		
		try {
			con = DbUtil.open();
			
			String sql = "UPDATE rezeptedatenbank.codetabelletree SET parent = ?, child = ? WHERE Codetabellentree_Id = ?;";
			pstmt = con.prepareStatement(sql);
			
			pstmt.setLong(1, codeTreeElement.getParent() );
			pstmt.setLong(2, codeTreeElement.getChild() );
			pstmt.setLong(3, codeTreeElement.getCodeTabellenTreeId());
			
			pstmt.executeUpdate();

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(con, pstmt, null, null);
		}
		
		return codeTreeElement;

	}
	
	/**
	 * L�schen einer Beziehung Kind-Eltern (Parent-Child) in die Datenbank
	 * @Param CodeTabelleTree codeTreeElement
	 */
	@Override
	public void delete(CodeTabelleTree codeTreeElement) {
		delete (codeTreeElement.getCodeTabellenTreeId());
	}

	/**
	 * L�schen einer Beziehung Kind-Eltern (Parent-Child) in die Datenbank
	 * @Param long CotabellenTreeNr
	 */
	@Override
	public void delete(Long codeTabellenTreeNr) {
		
		PreparedStatement pstmt = null;
		Connection con = null;
		
		try {
			con = DbUtil.open();
			String sql = "DELETE FROM rezeptedatenbank.codetabelletree WHERE Codetabellentree_Id = ?;";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, codeTabellenTreeNr);
			pstmt.executeUpdate();
		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(con, pstmt, null, null);
		}
	}
}



