package ch.gerberrenato.rezepte.services;

import java.util.List;

import ch.gerberrenato.rezepte.model.Zutat;

public interface ZutatenService {
	List<Zutat> getAllZutaten();
	List<Zutat> getZutatenRezept(long rezeptNr);
	Zutat add(Zutat element);
	Zutat update(Zutat element);
	void delete(Zutat element);
	void delete(Long ZutatenNr);
}
