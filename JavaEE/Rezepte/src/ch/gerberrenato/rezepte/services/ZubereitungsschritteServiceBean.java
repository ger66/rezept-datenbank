package ch.gerberrenato.rezepte.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.naming.NamingException;

import ch.gerberrenato.rezepte.model.Zubereitungsschritt;
import ch.gerberrenato.rezepte.util.DbUtil;

/**
 * Alle MEthoden die Zubereitungsschritte in der Datenbank aktualisieren oder hinzuf�gen.
 * So wie alle Methoden die Zubereitungsschritte aus der Datenbank lesen.
 * @author Renato Gerber
 *
 */
@Stateless
public class ZubereitungsschritteServiceBean implements ZubereitungsschritteService {

	private String sqlAllCodes = "SELECT ZubereitungsschrittNr, RezeptNr, ReihenfolgeNr,Beschreibung FROM rezeptedatenbank.zubereitungsschritte";
	private String sqlZubereitungsschrittForRezept = "SELECT ZubereitungsschrittNr, RezeptNr, ReihenfolgeNr,Beschreibung FROM rezeptedatenbank.zubereitungsschritte WHERE RezeptNr = ?";
		
	@Resource
	private SessionContext sessionContext;
	
	/**
	 * Erstellt eine Liste vom ResultSet
	 * @param ResultSet rs
	 * @return List<Zubereitungsschritt>
	 * @throws SQLException
	 */
	private List<Zubereitungsschritt> getListFromResultset(ResultSet rs) throws SQLException {
		
		List<Zubereitungsschritt> resList = new ArrayList<>();
		
		while (rs.next()) {
			// Retrieve by column name
			long zubereitungsschrittNr = rs.getLong("ZubereitungsschrittNr");
			long rezeptNr = rs.getLong("RezeptNr");
			long reihenfolgeNr = rs.getLong("ReihenfolgeNr");
			String beschreibung = rs.getString("Beschreibung");

			Zubereitungsschritt element = new Zubereitungsschritt(zubereitungsschrittNr, rezeptNr, reihenfolgeNr, beschreibung);
			resList.add(element);
		}
		return resList;
	}
	
	/**
	 * Hinzuf�gen eines Zubereitunsschrittes zur Datenbank
	 * @param Zubereitungsschritt element
	 * @return Zubereitungsschritt
	 */
	@Override
	public Zubereitungsschritt add(Zubereitungsschritt element) {

		PreparedStatement pstmt = null;
		Connection con = null;
		
		try {
			con = DbUtil.open();
			
			String sql = "INSERT INTO rezeptedatenbank.zubereitungsschritte (RezeptNr,ReihenfolgeNr,Beschreibung) VALUES (?,?,?);";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, element.getRezeptNr());
			pstmt.setLong(2, element.getReihenfolgeNr());
			pstmt.setString(3, element.getBeschreibung());
			pstmt.executeUpdate();

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(con, pstmt, null, null);
		}
		return element;
	}
	
	/**
	 * Aktualiserung des Zubereitungsschritts auf der Datenbank
	 * @param Zubereitungsschritt element
	 * @return Zubereitungsschritt
	 */
	@Override
	public Zubereitungsschritt update(Zubereitungsschritt element) {
		
		PreparedStatement pstmt = null;
		Connection con = null;
		
		try {
			con = DbUtil.open();
			
			String sql = "UPDATE rezeptedatenbank.zubereitungsschritte SET RezeptNr = ?,  ReihenfolgeNr = ?, Beschreibung = ?  WHERE ZubereitungsschrittNr = ?;";
			
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, element.getRezeptNr());
			pstmt.setLong(2, element.getReihenfolgeNr());
			pstmt.setString(3, element.getBeschreibung());
			pstmt.setLong(4, element.getZubereitungsschrittNr());
			pstmt.executeUpdate();

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(con, pstmt, null, null);
		}
		return element;
	}
	
	/**
	 * L�schen eines Zubereitungsschritts auf der Datenbank
	 * @param Zubereitungsschritt element
	 */
	@Override
	public void delete(Zubereitungsschritt element) {
		delete(element.getZubereitungsschrittNr());
	}
	
	/**
	 * L�schen eines Zubereitungsschritts auf der Datenbank
	 * @param long ZubereitungsschrittNr
	 */
	@Override
	public void delete(Long ZubereitungsschrittNr) {
		
		PreparedStatement pstmt = null;
		Connection con = null;
		
		try {
			con = DbUtil.open();
			
			String sql = "DELETE FROM rezeptedatenbank.zubereitungsschritte WHERE ZubereitungsschrittNr = ?;";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, ZubereitungsschrittNr);
			pstmt.executeUpdate();

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(con, pstmt, null, null);
		}
	}
	
	/**
	 * Git alle Zubereitungsschritte aus der Datenbank aus
	 * @return List<Zubereitungsschritt>
	 */
	@Override
	public List<Zubereitungsschritt> getAllZubereitungsschritte() {
		
		List<Zubereitungsschritt> resList = new ArrayList<>();
		
		Statement stmt = null;
		ResultSet rs = null;
		
		Connection con = null;
		
		try {
			con = DbUtil.open();
			
			stmt = con.createStatement();
			
			rs = stmt.executeQuery(sqlAllCodes);
			resList = getListFromResultset(rs);
			
		} catch (SQLException | NamingException e) {
			System.out.println("Select einfach");
			e.printStackTrace();
		} finally {
			DbUtil.close(con, null, stmt, rs);
		}

		return resList;
	}
	
	/**
	 * Git die Zubereitungsschritte f�r ein Rezept aus
	 * @param long RezeptNr
	 * @return List<Zubereitungsschritt>
	 */
	@Override
	public List<Zubereitungsschritt> getRezeptZubereitungsschritte(long RezeptNr) {
		
		List<Zubereitungsschritt> resList = new ArrayList<>();
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		Connection con = null;
		
		try {
			con = DbUtil.open();
			
			pstmt = con.prepareStatement(sqlZubereitungsschrittForRezept);
			pstmt.setLong(1, RezeptNr);
			rs = pstmt.executeQuery(); 
			resList = getListFromResultset(rs);
			
		} catch (SQLException | NamingException e) {
			System.out.println("Select komplex");
			e.printStackTrace();
		} finally {
			DbUtil.close(con, pstmt, null, rs);
		}
		
		return resList;
	}

}

