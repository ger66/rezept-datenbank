package ch.gerberrenato.rezepte.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.naming.NamingException;

import ch.gerberrenato.rezepte.model.Produkt;
import ch.gerberrenato.rezepte.util.DbUtil;

/**
 * Klasse die Daten von der Produkte Tabelle in der Datenbnak abzieht oder speichert.
 * @author Renato Gerber
 *
 */
@Stateless
public class ProdukteServiceBean implements ProdukteService {
	
	private String sqlAllCodes = "SELECT * FROM rezeptedatenbank.produkte Order by 2;";
	
	@Resource
	private SessionContext sessionContext;
	
	/**
	 * List die Daten von der Datenbank mit einem SQL.
	 * @param Stirng sql
	 * @return List<Produkt>
	 */
	private List<Produkt> getProdukteFormDb(String sql) {
		
		List<Produkt> resList = new ArrayList<>();
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		
		try {
			con = DbUtil.open();
			
			stmt = con.createStatement();
			
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				// Retrieve by column name
				long produkteNr = rs.getLong("produkteNr");
				String name = rs.getString("name");
				String beschreibung = rs.getString("beschreibung");
				double gewicht = rs.getDouble("gewicht");
				int einheit = rs.getInt("einheit");
				int tcmElement = rs.getInt("TcmElement");
				int kategorie = rs.getInt("kategorie");
				int activeFlag = rs.getInt("aktiv_f");
								
				Produkt produkt = new Produkt(produkteNr, name, beschreibung, gewicht, einheit, tcmElement, kategorie, activeFlag);
				resList.add(produkt);
			}

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(con, null, stmt, rs);
		}
		return resList;
	}
	
	/**
	 * F�gt ein Produkt in die Datenbank ein.
	 * @param Produkt element
	 * @return Produkt
	 */
	public Produkt add(Produkt element) {
		
		long codeElementId = 0;
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {
			con = DbUtil.open();
			
			String sql = "INSERT INTO rezeptedatenbank.produkte (ProdukteNr,Name,Beschreibung,Gewicht,Einheit,TcmElement,Kategorie, aktiv_f) VALUES(?,?,?,?,?,?,?,?);";
			pstmt = con.prepareStatement(sql);
			
			pstmt.setLong(1, element.getProduktNr());
			pstmt.setString(2, element.getName());
			pstmt.setString(3, element.getBeschreibung() );
			pstmt.setDouble(4, element.getGewicht());
			pstmt.setInt(5, element.getEinheit());
			pstmt.setInt(6, element.getTcmElement());
			pstmt.setInt(7, element.getKategorie());
			pstmt.setInt(8, element.getAktiv_f());
			
			codeElementId = pstmt.executeUpdate();

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(con, pstmt, null, null);
		}
		return element;
	}
	
	/**
	 * L�schen eines Produkts
	 * @param Produkt element
	 */
	public void delete(Produkt element) {
		delete(element.getProduktNr());
	}
	
	/**
	 * L�schen eines Produktes mit der ID
	 * @param long produktId
	 */
	@Override
	public void delete(Long produktNr) {
		
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {
			con = DbUtil.open();
			
			String sql = "DELETE FROM rezeptedatenbank.produkte WHERE ProdukteNr = ?";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, produktNr);
			pstmt.executeUpdate();

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(con, pstmt, null, null);
		}
	}
	/**
	 * Datensatz aktualisieren auf der Datenbank
	 * @param Produkt element
	 * @return Produkt
	 */
	public Produkt update(Produkt element) {
		
		int modifiedRows=0;
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = DbUtil.open();
			
			String sql = "UPDATE rezeptedatenbank.produkte SET Name = ?, Beschreibung = ?, Gewicht = ?, Einheit = ?, TcmElement = ?, Kategorie = ?, aktiv_f = ? WHERE ProdukteNr = ?;";
			
			pstmt = con.prepareStatement(sql);
			
			pstmt.setString(1, element.getName());
			pstmt.setString(2, element.getBeschreibung() );
			pstmt.setDouble(3, element.getGewicht());
			pstmt.setInt(4, element.getEinheit());
			pstmt.setInt(5, element.getTcmElement());
			pstmt.setInt(6, element.getKategorie());
			pstmt.setInt(7, element.getAktiv_f());
			pstmt.setLong(8, element.getProduktNr());
			
			modifiedRows = pstmt.executeUpdate();

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(con, pstmt, null, null);
		}

		return element;
	}

	/**
	 * Gibt alle Produkte als Liste aus
	 * @return List<Produkt>
	 */
	@Override
	public List<Produkt> getAllProdukts() {
		return getProdukteFormDb(sqlAllCodes);
	}

}

