package ch.gerberrenato.rezepte.services;

import java.util.List;

import ch.gerberrenato.rezepte.model.Zubereitungsschritt;

public interface ZubereitungsschritteService {
	List<Zubereitungsschritt> getAllZubereitungsschritte();
	List<Zubereitungsschritt> getRezeptZubereitungsschritte(long RezeptNr);
	Zubereitungsschritt add(Zubereitungsschritt element);
	Zubereitungsschritt update(Zubereitungsschritt element);
	void delete(Zubereitungsschritt element);
	void delete(Long ZubereitungsschrittNr);
}
