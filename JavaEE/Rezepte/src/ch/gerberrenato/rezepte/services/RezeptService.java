package ch.gerberrenato.rezepte.services;

import java.util.List;
import ch.gerberrenato.rezepte.model.Rezept;

public interface RezeptService {
	List<Rezept> getAllRezepte();
	Rezept add(Rezept rezept);
	void delete(Rezept rezept);
	Rezept update(Rezept rezept);
	void delete(Long rezeptNr);
	List<Rezept> getRezepteSearch(String suchText);
	List<Rezept> getRezepteSearch(long produkt1, long produkt2, long produkt3, long produkt4);
}
