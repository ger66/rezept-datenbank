package ch.gerberrenato.rezepte.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.naming.NamingException;

import ch.gerberrenato.rezepte.model.Zutat;
import ch.gerberrenato.rezepte.util.DbUtil;

/**
 * Alle Methoden f�r die Bearbeitung der Zutaten auf der Datenbank.
 * Alle Methoden die Zutaten zur�ck geben.
 * @author Renato Gerber
 *
 */
@Stateless
public class ZutatenServiceBean implements ZutatenService {
	// SQL's die zu lang waren f�r eine Methode wurden hier her ausgelagert.
	private String sqlAllCodes = "SELECT *, '' as ProduktBez, '' as EinheitBez FROM rezeptedatenbank.zutaten;";
	private String sqlZutatenForRezept = "SELECT zut.*, prod.Name as ProduktBez, ein.KurzBezeichnung as EinheitBez"
										+ " FROM rezeptedatenbank.zutaten zut INNER JOIN rezeptedatenbank.produkte prod on prod.ProdukteNr = zut.ProduktNr and prod.aktiv_f = 1"
										+ " INNER JOIN rezeptedatenbank.codetabelle ein on ein.CodetabelleNr = zut.EinheitNr and ein.aktiv_f = 1"
										+ " WHERE zut.RezeptNr = ?;";
	
	/**
	 * Gibt alle Zutaten aus der Datenbank aus
	 * @return List<Zutat> 
	 */
	@Override
	public List<Zutat> getAllZutaten() {
		
		List<Zutat> resList = new ArrayList<>();
		
		Statement stmt = null;
		ResultSet rs = null;
		Connection con = null;
		
		try {
			con = DbUtil.open();
			stmt = con.createStatement();
			
			rs = stmt.executeQuery(sqlAllCodes);
			resList = getListFromResultset(rs);
			
		} catch (SQLException | NamingException e) {
			System.out.println("Select einfach");
			e.printStackTrace();
		} finally {
			DbUtil.close(con, null, stmt, rs);
		}
			
		return resList;
	}
	
	/**
	 * Erstellt eine Lsite aus dem ResultSet
	 * @param ResultSet rs
	 * @return List<Zutat>
	 * @throws SQLException
	 */
	private List<Zutat> getListFromResultset(ResultSet rs) throws SQLException {
		
		List<Zutat> resList = new ArrayList<>();
		
		while (rs.next()) {
			// Retrieve by column name
			long zutatenNr = rs.getLong("ZutatenNr");
			long rezeptNr = rs.getLong("RezeptNr");
			long produktNr = rs.getLong("ProduktNr");
			String produktBez = rs.getString("ProduktBez");
			double menge = rs.getDouble("Menge");
			long einheitNr = rs.getLong("EinheitNr");
			String einheitBez = rs.getString("EinheitBez");

			Zutat element = new Zutat(zutatenNr, rezeptNr, produktNr, menge, einheitNr);
			element.setEinheitBez(einheitBez);
			element.setProduktBez(produktBez);
			resList.add(element);
		}
		return resList;
	}
	
	/**
	 * F�gt eine Zutat in die Datenbank ein.
	 * @param Zutat element
	 * @return Zutat
	 */
	@Override
	public Zutat add(Zutat element) {
		
		PreparedStatement pstmt = null;
		Connection con = null;
		
		try {
			con = DbUtil.open();	
			String sql = "INSERT INTO rezeptedatenbank.zutaten (RezeptNr, ProduktNr, menge, EinheitNr) VALUES (?,?,?,?);";
			pstmt = con.prepareStatement(sql);
			
			pstmt.setLong(1, element.getRezeptNr());
			pstmt.setLong(2, element.getProduktNr());
			pstmt.setDouble(3, element.getMenge());
			pstmt.setLong(4, element.getEinheitNr());
			pstmt.executeUpdate();

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(con, pstmt, null, null);
		}
		return element;
	}
	
	/**
	 * Aktualiser teine Zutat auf der Datenbank.
	 * @param Zutat element
	 * @return Zutat
	 */
	@Override
	public Zutat update(Zutat element) {
	
		PreparedStatement pstmt = null;
		Connection con = null;
		
		try {
			con = DbUtil.open();
			
			String sql = "UPDATE rezeptedatenbank.zutaten SET  RezeptNr = ?, ProduktNr = ? menge = ?, EinheitNr = ? WHERE ZutatenNr = ?;";
			
			pstmt = con.prepareStatement(sql);
			
			pstmt.setLong(1, element.getRezeptNr());
			pstmt.setLong(2, element.getProduktNr());
			pstmt.setDouble(3, element.getMenge());
			pstmt.setLong(4, element.getEinheitNr());
			pstmt.setLong(5, element.getZutatenNr());
			pstmt.executeUpdate();

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(con, pstmt, null, null);
		}

		return element;
	}
	
	/**
	 * L�schen einer Zutat auf der Datenbank
	 * @param Zutat element
	 */
	@Override
	public void delete(Zutat element) {
		delete(element.getZutatenNr());
	}
	
	/**
	 * L�schen einer Zutat auf der Datenbank
	 * @param long zutatenNr
	 */
	@Override
	public void delete(Long zutatenNr) {

		PreparedStatement pstmt = null;
		Connection con = null;
		
		try {
			con = DbUtil.open();
			
			String sql = "DELETE FROM rezeptedatenbank.zutaten WHERE ZutatenNr = ?;";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, zutatenNr);
			pstmt.executeUpdate();

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(con, pstmt, null, null);
		}
	}
	
	/**
	 * Gibt ide Zuten f�r ein Rezept zur�ck als Liste
	 * @param long rezeptNr
	 * @return List<Zutat>
	 */
	@Override
	public List<Zutat> getZutatenRezept(long rezeptNr) {
		
		List<Zutat> resList = new ArrayList<>();
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		Connection con = null;
		
		try {
			con = DbUtil.open();
			
			pstmt = con.prepareStatement(sqlZutatenForRezept);
			pstmt.setLong(1, rezeptNr);
			rs = pstmt.executeQuery(); 
			resList = getListFromResultset(rs);
			
		} catch (SQLException | NamingException e) {
			System.out.println("Select komplex");
			e.printStackTrace();
		} finally {
			DbUtil.close(con, pstmt, null, rs);
		}
		return resList;
	}

}

