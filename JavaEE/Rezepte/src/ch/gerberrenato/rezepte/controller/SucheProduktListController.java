package ch.gerberrenato.rezepte.controller;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import ch.gerberrenato.rezepte.data.RezepteListProducer;
import ch.gerberrenato.rezepte.model.Rezept;
import ch.gerberrenato.rezepte.services.RezeptService;

/**
 * Kotroller Klasse fpr die Suche nach Produkten
 * @author Renato Gerber
 *
 */
@SessionScoped
@Named
public class SucheProduktListController implements Serializable {
	private static final long serialVersionUID = -316852811523676920L;
	
	@Inject
	private RezeptService rezeptService;
	
	@Inject
	private RezepteListProducer rezeptProducer;
	
	private long produkt1;
	private long produkt2;
	private long produkt3;
	private long produkt4;

	/**
	 * Link zur Suche nach Produkten
	 * @return String Target-Ppage
	 */
	public String doList() {
		return Pages.SUCHE_PRODUKTE;
	}
	
	/**
	 * Rezepte suchen mit den Produkten aus den Auswahlboxen
	 * @return String Target-Ppage
	 */
	public String doSearch(){
		final List<Rezept> rez = rezeptService.getRezepteSearch(produkt1, produkt2, produkt3, produkt4);
		rezeptProducer.setRezepte(rez);
		return Pages.SUCHE_PRODUKTE;
	}
	
	public long getProdukt1() {
		return produkt1;
	}

	public void setProdukt1(long produkt1) {
		this.produkt1 = produkt1;
	}

	public long getProdukt2() {
		return produkt2;
	}

	public void setProdukt2(long produkt2) {
		this.produkt2 = produkt2;
	}

	public long getProdukt3() {
		return produkt3;
	}

	public void setProdukt3(long produkt3) {
		this.produkt3 = produkt3;
	}

	public long getProdukt4() {
		return produkt4;
	}

	public void setProdukt4(long produkt4) {
		this.produkt4 = produkt4;
	}
}
