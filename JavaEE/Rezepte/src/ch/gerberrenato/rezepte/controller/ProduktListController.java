package ch.gerberrenato.rezepte.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import ch.gerberrenato.rezepte.data.JaNeinFlagList;
import ch.gerberrenato.rezepte.model.Produkt;

/**
 * Kontroller Klasse f�r die Produkt Liste
 * @author Renato Gerber
 *
 */
@SessionScoped
@Named
public class ProduktListController implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Inject
	private ProduktEditController produktEditController;
	private JaNeinFlagList flag;
	
	/**
	 * Das Ja-Nein Flag muss nach der Erstellung des Kotroller erstellt werden.
	 * Da der Kontroller f�r eine Session g�ltig ist, wird das nur einmal ausgef�hrt.
	 */
	@PostConstruct
	public void init(){
		/* Flag Objekt inizialisieren */
		flag = new JaNeinFlagList();

	}
	
	/**
	 * Link zur �bersicht Produkte
	 * @return String Target-Ppage
	 */
	public String doList(){
		return Pages.PRODUKT_LIST;
	}
	
	/**
	 * Neues Produkt hinzuf�gen
	 * @return String Target-Ppage
	 */
	public String doAdd() {
		produktEditController.setProduktNew();
		return Pages.PRODUKT_EDIT;
	}

	/**
	 * Bestehendes Produkt bearbeiten
	 * @param Produkt element
	 * @return String Target-Ppage
	 */
	public String doEdit(Produkt element) {
		produktEditController.setProduktEdit( element);
		return Pages.PRODUKT_EDIT;
	}

	/**
	 * L�schen des Produktes
	 * @param Produkt produkt 
	 */
	public void doDelete(Produkt produkt) {
		produktEditController.doDelete(produkt);
	}

	/**
	 * Ja-Nein Flag �bersetzen
	 * @param int value
	 * @return Stirng
	 */
	public String getJaNeinFlag(int value){
		return flag.getJaNeinFlag(value);
	}
	
	
	public String getCodeTabelleBez(long value){
		return "" + value;
	}
}
