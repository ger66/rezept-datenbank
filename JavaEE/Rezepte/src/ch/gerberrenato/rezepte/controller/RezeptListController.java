package ch.gerberrenato.rezepte.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import ch.gerberrenato.rezepte.data.JaNeinFlagList;
import ch.gerberrenato.rezepte.data.RezeptDetailProducer;
import ch.gerberrenato.rezepte.model.Rezept;
import ch.gerberrenato.rezepte.model.Zubereitungsschritt;
import ch.gerberrenato.rezepte.model.Zutat;
import ch.gerberrenato.rezepte.services.ZubereitungsschritteService;
import ch.gerberrenato.rezepte.services.ZutatenService;

/**
 * Kontroller Klasse f�r die �bersicht der Rezepte
 * @author Renato Gerber
 *
 */
@SessionScoped
@Named
public class RezeptListController implements Serializable {
	private static final long serialVersionUID = -316852811523676920L;

	@Inject
	private RezeptEditController rezeptEditController;
	@Inject
	private ZutatenService zutatenService;
	@Inject
	private ZubereitungsschritteService zuberietungsSchritt;
	@Inject
	private RezeptDetailProducer rezeptDetailProducer;
	@Inject
	private RezeptDetailEditController rezeptDetailEdit;
	
	private JaNeinFlagList flag;
	
	/**
	 * Nach dem Erstellen des Beans soll die FlagListe auch schon geladen sein. 
	 */
	@PostConstruct
	public void init(){
		/* Flag Objekt inizialisieren */
		flag = new JaNeinFlagList();
	}

	/**
	 * �bersicht der Rezepte anzeigen
	 * @return String Target-Ppage
	 */
	public String doList() {
		return Pages.REZEPT_LIST;
	}

	/**
	 * Neues Rezept erstellen
	 * @return String Target-Ppage
	 */
	public String doAddRezept() {
		rezeptEditController.setRezeptNew();
		return Pages.REZEPT_EDIT;
	}

	/**
	 * Bestehendes Rezept bearbeiten
	 * @param Rezept rezept
	 * @return String Target-Ppage
	 */
	public String doEditRezept(Rezept rezept) {
		rezeptEditController.setRezeptEdit(rezept);
		return Pages.REZEPT_EDIT;
	}
	
	/**
	 * Rezept l�schen
	 * @param Rezept rezept
	 */
	public void doDeleteRezept(Rezept rezept) {
		rezeptEditController.doDelete(rezept);
	}
	
	/**
	 * Rezepte Details View die editiert werden kann
	 * @param Rezept rezept
	 * @return String Target-Ppage
	 */
	public String doShowDetailsWithEdit(Rezept rezept, String page) {
		return doShowDetails(rezept, true, page);
	}
	
	/**
	 * Rezepte Detail View die nicht editiert werden kann
	 * @param Rezept rezept
	 * @return String Target-Ppage
	 */
	public String doShowDetailsNoEdit(Rezept rezept, String page){
		return doShowDetails(rezept, false, page);
	}
	
	/**
	 * Wrapper f�r die doShowDetailsNoEdit und doShowDetailsEdit Methode
	 * @param Rezept rezept
	 * @param boolean edit
	 * @return String Target-Ppage
	 */
	private String doShowDetails(Rezept rezept, boolean edit, String page){
		
		// Page auswerten
		switch (page) {
		case "SUCHE_EINFACH":
			page = Pages.SUCHE_EINFACH;
			break;
		case "SUCHE_PRODUKT":
			page = Pages.SUCHE_PRODUKTE;
			break;
		case "REZEPTDETAIL_LIST":
			page = Pages.REZEPT_LIST;
			break;
		default:
			page = Pages.REZEPT_LIST;
			break;
		}

		/* Die n�tigen Daten laden f�r das Rezept */
		final List<Zutat> zutatenListe = zutatenService.getZutatenRezept(rezept.getRezeptNr());
		rezeptDetailProducer.setRezeptZutatenList(zutatenListe);

		final List<Zubereitungsschritt> zubereitungsSchrittList = zuberietungsSchritt.getRezeptZubereitungsschritte(rezept.getRezeptNr());
		rezeptDetailProducer.setZubereitungsSchrittList(zubereitungsSchrittList);
		rezeptDetailProducer.setRezept(rezept);

		rezeptDetailEdit.setEdit(edit);
		rezeptDetailEdit.setLandingPage(page);
		
		return Pages.REZEPTDETAIL_LIST;
	}
	
	/**
	 * Link zu Home-Site
	 * @return string
	 */
	public String doHome(){
		return Pages.HOME;
	}
	
	/**
	 * Gibt Ja / Nein Flag Text zur�ck
	 * @param int JaNein Flag
	 * @return string
	 */
	public String getJaNeinFlag(int value){
		return flag.getJaNeinFlag(value);
	}
	
}
