package ch.gerberrenato.rezepte.controller;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import ch.gerberrenato.rezepte.data.RezepteListProducer;
import ch.gerberrenato.rezepte.model.Rezept;
import ch.gerberrenato.rezepte.services.RezeptService;

/**
 * Kontroller Klasse f�r die einfache Suche nach Rezepten
 * @author Renato Gerber
 *
 */
@SessionScoped
@Named
public class SucheEinfachListController implements Serializable {
	private static final long serialVersionUID = -316852811523676920L;
	
	@Inject
	private RezeptService rezeptService;
	
	@Inject
	private RezepteListProducer rezeptProducer;
	
	private String suchText;

	/**
	 * Link auf die �bersicht Suche nach Rezepten 
	 * @return String Target-Ppage
	 */
	public String doList() {
		return Pages.SUCHE_EINFACH;
	}
	
	/**
	 * Suche nach Rezepten die den Suchtext enthalten
	 * @return String Target-Ppage
	 */
	public String doSearch(){
		final List<Rezept> rez = rezeptService.getRezepteSearch(suchText);
		rezeptProducer.setRezepte(rez);
		return Pages.SUCHE_EINFACH;
	}
	
	public String getSuchText() {
		return suchText;
	}

	public void setSuchText(String suchText) {
		this.suchText = suchText;
	}
}
