package ch.gerberrenato.rezepte.controller;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;

import ch.gerberrenato.rezepte.data.JaNeinFlagList;
import ch.gerberrenato.rezepte.model.FlagItem;
import ch.gerberrenato.rezepte.model.Rezept;
import ch.gerberrenato.rezepte.util.Events.Added;
import ch.gerberrenato.rezepte.util.Events.Deleted;
import ch.gerberrenato.rezepte.util.Events.Updated;

/**
 * Kontroller Klasse f�r die bearbeitung der Rezepte
 * @author Renato Gerber
 *
 */
@SessionScoped
@Named
public class RezeptEditController implements Serializable {
	private static final long serialVersionUID = 1L;

	@Inject
	@Added
	private Event<Rezept> rezeptAddEventSrc;

	@Inject
	@Updated
	private Event<Rezept> rezeptUpdatedEventSrc;

	@Inject
	@Deleted
	private Event<Rezept> rezeptDeleteEventSrc;
	
	public enum Mode {
		EDIT, ADD
	};

	private Rezept rezept;
	private Mode mode;
	

	private Mode getMode() {
		return mode;
	}

	public Rezept getRezept() {
		return rezept;
	}

	/**
	 * Neues Rezept erstellen,
	 * Es wird der Mode auf ADD gesetzt,
	 * und ein neues Rezept Objekt erstellt
	 */
	public void setRezeptNew() {
		setRezept(Mode.ADD, new Rezept());
	}

	/**
	 * Bestehendes Rezept bearbeiten
	 * @param Rezept rezept
	 */
	public void setRezeptEdit( Rezept rezept) {
		setRezept(Mode.EDIT, rezept);
	}

	/**
	 * Wrapper Methode f�r die beiden Methoden setRezeptEdit und setRezeptNew
	 * @param Mode mode
	 * @param Rezept rezept
	 */
	private void setRezept(Mode mode, Rezept rezept) {
		this.rezept = rezept;
		this.mode = mode;
	}
	
	/**
	 * Speichern des Rezeptes
	 * @return String Target-Ppage
	 */
	public String doSave() {
		if (getMode() == Mode.ADD) {
			rezeptAddEventSrc.fire(rezept);
		} else {
			rezeptUpdatedEventSrc.fire(rezept);
		}
		return Pages.REZEPT_LIST;
	}

	/**
	 * View verlassen ohne speichern
	 * @return String Target-Ppage
	 */
	public String doCancel() {
		return Pages.REZEPT_LIST;
	}
	
	/**
	 * Rezept l�schen
	 * @param Rezept rezept
	 */
	public void doDelete(Rezept rezept){
		rezeptDeleteEventSrc.fire(rezept);
	}

	/**
	 * Gibt den Titel aus, je nach Mode
	 * @return String
	 */
	public String getTitle() {
		return getMode() == Mode.EDIT ? "rezept.RezeptdatenEditieren" : "rezept.NeuesRezeptAnlegen";
	}
	
	/**
	 * Flag f�r Ja Nein Attribute
	 * @return FlagItem[]
	 */
	public FlagItem[] getJaNeinFlag() {
		JaNeinFlagList flag = new JaNeinFlagList();
		return flag.getJaNeinFlag();
	}
	
}
