package ch.gerberrenato.rezepte.controller;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;

import ch.gerberrenato.rezepte.data.JaNeinFlagList;
import ch.gerberrenato.rezepte.model.CodeTabelle;
import ch.gerberrenato.rezepte.model.FlagItem;
import ch.gerberrenato.rezepte.util.Events.Added;
import ch.gerberrenato.rezepte.util.Events.Deleted;
import ch.gerberrenato.rezepte.util.Events.Updated;

/**
 * Controller Klasse f�r das Editieren der Codetabelle
 * @author Renato Gerber
 *
 */
@SessionScoped
@Named
public class CodeTableEditController implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Inject
	private CodeTableTreeEditController codeTreeEditController; 
	
	@Inject
	@Added
	private Event<CodeTabelle> codeTabelleAddEventSrc;
	
	@Inject
	@Updated
	private Event<CodeTabelle> codeTabelleUpdatedEventSrc;
	
	@Inject
	@Deleted
	private Event<CodeTabelle> codeTabelleDeleteEventSrc;
	
	public enum Mode {
		EDIT, ADD
	};

	private CodeTabelle codeTabelle;
	private Mode mode;
	
	public CodeTabelle getCodeTabelle(){
		return codeTabelle;
	}
			
	public Mode getMode(){
		return mode;
	}
	
	/**
	 * Neuer Codetabellen Eintrag erstellen
	 * Der Mode wird auf ADD gesetzt.
	 * Dazu wird noch ein leeres CodeTabellen Element mit �bergeben.
	 */
	public void setCodeTabelleNew() {
		setCodeTabelle(Mode.ADD, new CodeTabelle());
	}
	
	/**
	 * Bestehendes Element setzen, dass es gespeichert werden kann.
	 * Der Mode wird auf Edit gesetzt
	 * @param CodeTabelle codeElement 
	 */
	public void setCodeTabelleEdit(CodeTabelle codeElement) {
		setCodeTabelle(Mode.EDIT, codeElement);
	}
	
	/**
	 * Funktion die als Wrapper dient f�r setCodeTabelleEdit und setCodeTabelleNew
	 * @param Mode mode
	 * @param CodeTabelle codeElement
	 */
	private void setCodeTabelle(Mode mode, CodeTabelle codeElement) {
		this.codeTabelle = codeElement;
		this.mode = mode;
	}
	
	/**
	 * Speichern des Elementes
	 * @return String Target-Ppage
	 */
	public String doSave() {
		if (getMode() == Mode.ADD) {
			codeTabelleAddEventSrc.fire(codeTabelle);
		} else {
			codeTabelleUpdatedEventSrc.fire(codeTabelle);
		}
		return Pages.CODETABLE_LIST;
	}
	
	/**
	 * Git den Title zur�ck, f�r das JSF, je nach Mode.
	 * @return String
	 */
	public String getTitle() {
		return getMode() == Mode.EDIT ? "codeTabelle.CodetabelleEditieren" : "codeTabelle.NeuerCodeErfassen";
	}
	
	/**
	 * Seite verlassen ohne speichern
	 * @return String Target-Ppage
	 */
	public String doCancel() {
		return Pages.CODETABLE_LIST;
	}
	
	/**
	 * Datensatz l�scchen
	 * @param CodeTabelle codeTabelle
	 */
	public void doDelete(CodeTabelle codeTabelle) {
		codeTabelleDeleteEventSrc.fire(codeTabelle);
	}
	
	/**
	 * Neues Kinde-Element zu dem Element anf�gen
	 * @return String Target-Ppage
	 */
	public String doNewChild(){
		codeTreeEditController.setCodeTreeElementNew(codeTabelle);
		return Pages.CODETABLETREE_EDIT;
	}
	
	/**
	 * Bestehendes Kind-Element bearbeiten
	 * @param CodeTabelle element
	 * @return String Target-Ppage
	 */
	public String doEditChild(CodeTabelle element){
		codeTreeEditController.setCodeTreeElementEdit(element, codeTabelle);
		return Pages.CODETABLETREE_EDIT;
	}
	
	/**
	 * Kin-Element l�schen
	 * @param CodeTablle element
	 */
	public void doDeleteChild(CodeTabelle element){
		codeTreeEditController.doDelete(element);
	}
	
	/**
	 * Funktion die das Ja-Nein Flag setzt
	 * @return FlagItem[]
	 */
	public FlagItem[] getJaNeinFlag() {
		JaNeinFlagList flag = new JaNeinFlagList();
		return flag.getJaNeinFlag();
	}
	
}
