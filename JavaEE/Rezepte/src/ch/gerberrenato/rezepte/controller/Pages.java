package ch.gerberrenato.rezepte.controller;

/**
 * Alle Linkt zu den Pages als Static Strings, so dass diese von �berall in der App aufgerufen werden k�nnen.
 * @author Renato Gerber
 *
 */
public class Pages {
	public static final String HOME = "/index.xhtml";
	
	public static final String REZEPT_LIST = "/rezepte/rezeptuebersicht.xhtml";
	public static final String REZEPT_EDIT = "/rezepte/rezepterfassen.xhtml";
	
	public static final String REZEPTDETAIL_LIST = "/rezepte/rezeptDetails.xhtml";
	public static final String REZEPTDETAIL_EDIT= "/rezepte/rezeptDetailsErfassen.xhtml";
	public static final String REZEPTDETAL_ZUTATERFASSEN = "/rezepte/rezeptZutatErfassen.xhtml";
	public static final String REZEPTDETAL_SCHRITT= "/rezepte/rezeptZubereitungsSchritt.xhtml";
	
	public static final String CODETABLE_LIST = "/codetabellen/codetabelleuebersicht.xhtml";
	public static final String CODETABLE_EDIT = "/codetabellen/codetabelleerfassen.xhtml";
	
	public static final String CODETABLETREE_EDIT = "/codetabellen/codetabelletreeerfassen.xhtml";
	
	public static final String PRODUKT_LIST = "/codetabellen/produktuebersicht.xhtml";
	public static final String PRODUKT_EDIT = "/codetabellen/produkterfassen.xhtml";
	
	public static final String SUCHE_EINFACH = "/suche/einfacheSuche.xhtml";
	public static final String SUCHE_PRODUKTE = "/suche/produktenSuche.xhtml";
	
}
