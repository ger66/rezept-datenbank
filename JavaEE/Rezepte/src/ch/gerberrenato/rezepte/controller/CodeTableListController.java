package ch.gerberrenato.rezepte.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import ch.gerberrenato.rezepte.data.JaNeinFlagList;
import ch.gerberrenato.rezepte.model.CodeTabelle;

/**
 * Codetabellen �bersicht Controller
 * @author Renato Gerber
 *
 */
@SessionScoped
@Named
public class CodeTableListController implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Inject
	private CodeTableEditController codeTabelleEditController;
	private JaNeinFlagList flag;
	
	/**
	 * Finktion die die Daten von der DB list
	 */
	@PostConstruct
	public void init(){
		/* Flag Objekt inizialisieren */
		flag = new JaNeinFlagList();
	}

	/**
	 * Gibt die Target-Page zur�ck f�r die �bersicht
	 * @return String Target-Ppage
	 */
	public String doList(){
		return Pages.CODETABLE_LIST;
	}
	
	/**
	 * Neues Element hinzuf�gen
	 * @return String Target-Ppage
	 */
	public String doAdd() {
		codeTabelleEditController.setCodeTabelleNew();
		return Pages.CODETABLE_EDIT;
	}
	
	/**
	 * Element deitieren
	 * @param CodeTabelle codeElement
	 * @return String Target-Ppage
	 */
	public String doEdit(CodeTabelle codeElement) {
		codeTabelleEditController.setCodeTabelleEdit(codeElement);
		return Pages.CODETABLE_EDIT;
	}
	
	/**
	 * Eintrag l�schen
	 * @param CodeTabelle codeTabelle
	 */
	public void doDelete(CodeTabelle codeTabelle) {
		codeTabelleEditController.doDelete(codeTabelle);
	}

	/**
	 * Gibt das Ja-Nein Flag in der �bersicht aus
	 * @param int value 
	 * @return String
	 */
	public String getJaNeinFlag(int value){
		return flag.getJaNeinFlag(value);
	}

}
