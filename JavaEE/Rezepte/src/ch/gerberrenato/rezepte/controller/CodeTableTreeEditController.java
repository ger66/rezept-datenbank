package ch.gerberrenato.rezepte.controller;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;

import ch.gerberrenato.rezepte.model.CodeTabelle;
import ch.gerberrenato.rezepte.model.CodeTabelleTree;
import ch.gerberrenato.rezepte.util.Events.Added;
import ch.gerberrenato.rezepte.util.Events.Deleted;
import ch.gerberrenato.rezepte.util.Events.Updated;

/**
 * Code-Parent-Child verbindungen Editieren
 * @author Renato Gerber
 *
 */
@SessionScoped
@Named
public class CodeTableTreeEditController implements Serializable {
	private static final long serialVersionUID = 1L;

	@Inject
	@Added
	private Event<CodeTabelleTree> codeTabelleTreeAddEventSrc;
	
	@Inject
	@Updated
	private Event<CodeTabelleTree> codeTabelleTreeUpdatedEventSrc;
	
	@Inject
	@Deleted
	private Event<CodeTabelleTree> codeTabelleTreeDeleteEventSrc;
	
	@Inject
	@Updated
	private Event<CodeTabelle> codeTabelleUpdatedEventSrc;
	
	public enum Mode {
		EDIT, ADD
	};

	private CodeTabelleTree codeTreeElement;
	private CodeTabelle codeElement;
	private Mode mode;

	public long getNewCodeElement() {
		return codeTreeElement.getChild();
	}

	public void setNewCodeElement(long newCodeElement) {
		codeTreeElement.setChild(newCodeElement);
	}
	
	public CodeTabelleTree getCodeTreeElement(){
		return codeTreeElement;
	}

	public CodeTabelle getCodeElement() {
		return codeElement;
	}
	
	public Mode getMode(){
		return mode;
	}

	/**
	 * Neues Kind-Element sertzten
	 * @param CodeTabelle codeTableParent
	 */
	public void setCodeTreeElementNew(CodeTabelle codeTableParent) {
		setCcodeTreeElement(Mode.ADD, new CodeTabelle(), codeTableParent);
	}
	
	/**
	 * Bestehendes Kind-Element bearbeiten
	 * @param CodeTabelle Kind-Element
	 * @param CodeTabelle Parent-Element
	 */
	public void setCodeTreeElementEdit(CodeTabelle codeTreeElement, CodeTabelle codeTableParent) {
		setCcodeTreeElement(Mode.EDIT, codeTreeElement, codeTableParent);
	}
	
	/**
	 * Private Wrapper MEthode f�r das bearbeiten der Kind-Elemente 
	 * @param Mode mode
	 * @param CodeTabelle Kind-Element
	 * @param CodeTabelle Parent-Element
	 */
	private void setCcodeTreeElement(Mode mode, CodeTabelle codeTreeElement, CodeTabelle codeTableParent) {
		CodeTabelleTree elem = new CodeTabelleTree(codeTreeElement.getCodeTabelleTreeId(), codeTableParent.getCodeTabelleNr(), codeTreeElement.getCodeTabelleNr());
		this.codeTreeElement = elem;
		this.codeElement = codeTableParent;
		this.mode = mode;
	}
	
	/**
	 * Speichern der Bezeihung Kind-Eltern Element
	 * @return String Target-Ppage
	 */
	public String doSave() {
		if (getMode() == Mode.ADD) {
			codeTabelleTreeAddEventSrc.fire(codeTreeElement);
		} else {
			codeTabelleTreeUpdatedEventSrc.fire(codeTreeElement);
		}
		
		// CodeElement neu laden im Speicher
		codeTabelleUpdatedEventSrc.fire(codeElement);
		
		return Pages.CODETABLE_EDIT;
	}
	
	/**
	 * Git den Title aus, je nach Mode
	 * @return String
	 */
	public String getTitle() {
		return getMode() == Mode.EDIT ? "codeTabelleTree.CodeTabelleTreeEditieren" : "codeTabelleTree.NeuerCodeTreeErfassen";
	}
	
	/**
	 * View verlassen ohne speichern
	 * @return String Target-Ppage
	 */
	public String doCancel() {
		return Pages.CODETABLE_EDIT;
	}
	
	/**
	 * Verbindung Kind- Eltern Element l�schen
	 * @param Codtabelle element
	 */
	public void doDelete(CodeTabelle element){
		CodeTabelleTree elem = new CodeTabelleTree(element.getCodeTabelleTreeId(),0L,0L);
		codeTabelleTreeDeleteEventSrc.fire(elem);
	}
}
