package ch.gerberrenato.rezepte.controller;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;

import ch.gerberrenato.rezepte.data.JaNeinFlagList;
import ch.gerberrenato.rezepte.model.FlagItem;
import ch.gerberrenato.rezepte.model.Produkt;
import ch.gerberrenato.rezepte.util.Events.Added;
import ch.gerberrenato.rezepte.util.Events.Deleted;
import ch.gerberrenato.rezepte.util.Events.Updated;

/**
 * Controller Klasse f�r die bearbeitung der Produkte
 * @author Renato Gerber
 *
 */
@SessionScoped
@Named
public class ProduktEditController implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Inject
	@Added
	private Event<Produkt> produktAddEventSrc;
	
	@Inject
	@Updated
	private Event<Produkt> produktUpdatedEventSrc;
	
	@Inject
	@Deleted
	private Event<Produkt> produktDeleteEventSrc;
	
	public enum Mode {
		EDIT, ADD
	};
	
	private Produkt produkt;
	private Mode mode;
	
	
	public Produkt getProdukt(){
		return produkt;
	}
			
	public Mode getMode(){
		return mode;
	}
	
	/**
	 * Neues Produkt erstellen
	 */
	public void setProduktNew() {
		setProdukt(Mode.ADD, new Produkt());
	}
	
	/**
	 * Bestehendes Produkt vearbeiten
	 * @param Produkt element
	 */
	public void setProduktEdit(Produkt element) {
		setProdukt(Mode.EDIT, element);
	}
	
	/**
	 * Wrapper Methode f�r die Produkt bearbeitung
	 * @param Mode mode
	 * @param Produkt element
	 */
	private void setProdukt(Mode mode, Produkt element) {
		this.produkt = element;
		this.mode = mode;
	}
	
	/**
	 * Speichern des Produktes
	 * @return String Target-Ppage
	 */
	public String doSave() {
		if (getMode() == Mode.ADD) {
			produktAddEventSrc.fire(produkt);
		} else {
			produktUpdatedEventSrc.fire(produkt);
		}
		return Pages.PRODUKT_LIST;
	}
	
	/**
	 * Titel ausgeben, je nach Mode
	 * @return String
	 */
	public String getTitle() {
		return getMode() == Mode.EDIT ? "produkt.ProduktEditieren" : "produkt.NeuerErfassen";
	}
	
	/**
	 * View verlassen ohne speichern
	 * @return String Target-Ppage
	 */
	public String doCancel() {
		return Pages.PRODUKT_LIST;
	}
	/**
	 * Produkt l�schen
	 * @param Produkt produkt
	 */
	public void doDelete(Produkt produkt) {
		produktDeleteEventSrc.fire(produkt);
	}
	
	/**
	 * Flag f�r �bersicht 
	 * @return string
	 */
	public FlagItem[] getJaNeinFlag() {
		JaNeinFlagList flag = new JaNeinFlagList();
		return flag.getJaNeinFlag();
	}
	
}
