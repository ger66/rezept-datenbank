package ch.gerberrenato.rezepte.controller;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;

import ch.gerberrenato.rezepte.model.Rezept;
import ch.gerberrenato.rezepte.model.Zubereitungsschritt;
import ch.gerberrenato.rezepte.model.Zutat;
import ch.gerberrenato.rezepte.util.Events.Added;
import ch.gerberrenato.rezepte.util.Events.Deleted;

/**
 * Kontroller Klasse f�r die Rezepte Details
 * @author Renato Gerber
 *
 * Hier werden alle Daten f�r das Rezept im Kontroller gespeichert.
 */
@SessionScoped
@Named
public class RezeptDetailEditController implements Serializable {
	private static final long serialVersionUID = 1L;

	@Inject
	@Deleted
	private Event<Zutat> zutatDeleteEventSrc;
	
	@Inject
	@Deleted
	private Event<Zubereitungsschritt> zubereitungsSchrittDeleteEventSrc;
	
	@Inject
	@Added
	private Event<Zubereitungsschritt> zubereitungsSchrittAddEventSrc;
	
	@Inject
	@Added
	private Event<Zutat> zutatAddEventSrc;
	
	private Zutat zutat;
	private Zubereitungsschritt zubereitungsSchritt;
	private Rezept rezept;
	private boolean edit;
	private String landingPage;
	
	/**
	 * Zuzat l�schen
	 * @param Zutat zutat
	 * @return String Target-Ppage
	 */
	public String doDeleteZutat(Zutat zutat){
				zutatDeleteEventSrc.fire(zutat);
		return Pages.REZEPTDETAIL_LIST;
	}
	
	/**
	 * Zubereitungsschritt l�schen
	 * @param Zubereitungsschritt schritt
	 * @return String Target-Ppage
	 */
	public String doDeletSchritt(Zubereitungsschritt schritt){
		zubereitungsSchrittDeleteEventSrc.fire(schritt);
		return Pages.REZEPTDETAIL_LIST;
	}
	
	/**
	 * Zutat zum Rezept hinzuf�gen
	 * @param Rezept rezept
	 * @return String Target-Ppage
	 */
	public String doAddRezeptZutat(Rezept rezept){
		this.rezept = rezept;
		this.zutat = new Zutat();
		this.zutat.setRezeptNr(rezept.getRezeptNr());
		return Pages.REZEPTDETAL_ZUTATERFASSEN;
	}
	
	/**
	 * Zubereitungsshcritt hinzuf�gen zum Rezept
	 * @param Rezept rezept
	 * @return String Target-Ppage
	 */
	public String doAddRezeptSchritt(Rezept rezept){
		this.rezept = rezept;
		this.zubereitungsSchritt = new Zubereitungsschritt();
		this.zubereitungsSchritt.setRezeptNr(rezept.getRezeptNr());
		return Pages.REZEPTDETAL_SCHRITT;
	}
	
	/**
	 * Zutat speichern mit dem Rezept
	 * @return String Target-Ppage
	 */
	public String doSaveZutat(){
		zutatAddEventSrc.fire(zutat);
		return Pages.REZEPTDETAIL_LIST;
	}
	
	/**
	 * Zubereitungschritt speichern mit dem Rezept
	 * @return String Target-Ppage
	 */
	public String doSaveSchritt(){
		zubereitungsSchrittAddEventSrc.fire(zubereitungsSchritt);
		return Pages.REZEPTDETAIL_LIST;
	}
	
	/**
	 * View verlassen, ohne zu speichern
	 * @return String Target-Ppage
	 */
	public String doCancel(){
		return Pages.REZEPTDETAIL_LIST;
	}
	
	/**
	 * View verlassen, ohne zu speichern
	 * die landingpage wird vom aufrufer gesetzt
	 * @return String Target-Ppage
	 */
	public String doCancelDetails(){
		return this.landingPage;
	}
	
	public Zutat getZutat() {
		return zutat;
	}

	public void setZutat(Zutat zutat) {
		this.zutat = zutat;
	}

	public Rezept getRezept() {
		return rezept;
	}

	public void setRezept(Rezept rezept) {
		this.rezept = rezept;
	}

	public Zubereitungsschritt getZubereitungsSchritt() {
		return zubereitungsSchritt;
	}

	public void setZubereitungsSchritt(Zubereitungsschritt zubereitungsSchritt) {
		this.zubereitungsSchritt = zubereitungsSchritt;
	}
	
	public boolean isEdit() {
		return edit;
	}

	public void setEdit(boolean edit) {
		this.edit = edit;
	}

	public void setLandingPage(String landingPage) {
		this.landingPage = landingPage;
	}
}
