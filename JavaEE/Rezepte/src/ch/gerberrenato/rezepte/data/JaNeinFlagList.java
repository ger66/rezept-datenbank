package ch.gerberrenato.rezepte.data;

import java.io.Serializable;

import ch.gerberrenato.rezepte.model.FlagItem;
/**
 * Einfache Prdoucer Klasse f�r die Ja Nein Flags.
 * @author Renato Gerber
 *
 */
public class JaNeinFlagList implements Serializable {
	private static final long serialVersionUID = 1L;

	private FlagItem[] aktivFlag;
	
	public JaNeinFlagList(){
		
		aktivFlag = new FlagItem[2];
		aktivFlag[0] = new FlagItem("default.AktivFlagJa", 1);
		aktivFlag[1] = new FlagItem("default.AktivFlagNein", 2);
	}

	
	public FlagItem[] getJaNeinFlag(){
		return aktivFlag;
	}
	
	public String getJaNeinFlag(int value){
		
		for (int i = 0; i < aktivFlag.length; i++) {
			if (value == aktivFlag[i].value) {
				return aktivFlag[i].bezeichnung;
			}
		}
		return "kein Wert";
	}
}
