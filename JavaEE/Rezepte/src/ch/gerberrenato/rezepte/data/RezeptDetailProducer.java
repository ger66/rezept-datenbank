package ch.gerberrenato.rezepte.data;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import ch.gerberrenato.rezepte.model.Rezept;
import ch.gerberrenato.rezepte.model.Zubereitungsschritt;
import ch.gerberrenato.rezepte.model.Zutat;
import ch.gerberrenato.rezepte.services.ZubereitungsschritteService;
import ch.gerberrenato.rezepte.services.ZutatenService;

/**
 * Klasse die alle Daten zu den Details eines Rezeptes ausgibt.
 * @author Renato Gerber
 *
 */
@SessionScoped
public class RezeptDetailProducer implements Serializable {
	private static final long serialVersionUID = 1L;

	private Rezept rezept;
	private List<Zutat> zutatenList;
	private List<Zubereitungsschritt> zubereitungsSchritteList;

	@Inject
	private ZutatenService zutatenService;
	
	@Inject
	private ZubereitungsschritteService zubereitungsService;
	
	/**
	 * Alle Zutaten f�r ein Rezept ausgeben
	 * @return List<Zutat>
	 */
	@Produces
	@Named
	public List<Zutat> getRezeptZutaten() {
		
		if (rezept != null){
			zutatenList = zutatenService.getZutatenRezept(rezept.getRezeptNr());
		}
		return zutatenList;
	}
	
	/**
	 * Alle Zubereitungsschritte f�r ein Rezept ausgeben 
	 * @return List<Zubereitungsschritt>
	 */
	@Produces
	@Named
	public List<Zubereitungsschritt> getRezeptZubereitungsSchritte(){
		if (rezept != null){
			zubereitungsSchritteList = zubereitungsService.getRezeptZubereitungsschritte(rezept.getRezeptNr());
		}
		return zubereitungsSchritteList;		
	}
	
	/**
	 * gibt das aktuelle Rezept aus, f�r das die Zutaten und Zubereitungsschritte geladen sind.
	 * @return Rezept
	 */
	@Produces
	@Named
	public Rezept getRezeptDetail() {
		return rezept;
	}
	
	public void setRezeptZutatenList(List<Zutat> zutatenList) {
		this.zutatenList = zutatenList;
	}

	public void setRezept(Rezept rezept) {
		this.rezept = rezept;
	}
	
	public void setZubereitungsSchrittList(List<Zubereitungsschritt> zubereitung){
		this.zubereitungsSchritteList = zubereitung;
	}

}
