package ch.gerberrenato.rezepte.data;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import ch.gerberrenato.rezepte.model.Produkt;
import ch.gerberrenato.rezepte.services.ProdukteService;
import ch.gerberrenato.rezepte.util.Events.Added;
import ch.gerberrenato.rezepte.util.Events.Deleted;
import ch.gerberrenato.rezepte.util.Events.Updated;

/**
 * Klasse die alle Daten zum Produkt ausgibt und die Events zum Produkt enth�lt.
 * @author Renato Gerber 
 *
 */
@RequestScoped
public class ProduktListProducer {
	
	private List<Produkt> produktList;

	@Inject
	private ProdukteService produktService;
	
	/**
	 * Produkt Liste inizialisieren
	 */
	@PostConstruct
	public void init() {
		produktList = produktService.getAllProdukts();
	}
	
	/**
	 * Gibt alle Produkte aus
	 * @return List<Produkt>
	 */
	@Produces
	@Named
	public List<Produkt> getProduktList() {
		return produktList;
	}
	
	/**
	 * Event zum hinzuf�gen eines Produkts
	 * @param Produkt element
	 */
	public void onelementAdded(@Observes @Added Produkt element) {
		produktService.add(element);
		init();
	}

	/**
	 * Event zum l�schen eines Produkts
	 * @param Produkt element
	 */
	public void onelementDeleted(@Observes @Deleted Produkt element) {
		produktService.delete(element);
		init();
	}

	/**
	 * Event zum aktualisieren eines Produkts
	 * @param Produkt element
	 */
	public void onelementUpdated(@Observes @Updated Produkt element) {
		produktService.update(element);
		init();
	}

}
