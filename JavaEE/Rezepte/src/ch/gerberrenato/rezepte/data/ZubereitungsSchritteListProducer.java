package ch.gerberrenato.rezepte.data;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import ch.gerberrenato.rezepte.model.Zubereitungsschritt;
import ch.gerberrenato.rezepte.services.ZubereitungsschritteService;
import ch.gerberrenato.rezepte.util.Events.Added;
import ch.gerberrenato.rezepte.util.Events.Deleted;
import ch.gerberrenato.rezepte.util.Events.Updated;
/**
 * Kalsse f�r alle Zubereitungsschritt Methoden
 * @author Renato Gerber
 *
 */
@RequestScoped
public class ZubereitungsSchritteListProducer {
	
	@Inject
	private ZubereitungsschritteService zubereitungsschritteService;
	
	/**
	 * Event f�r das hinzuf�gen eines Zubereitungsschrittes.
	 * @param Zubereitungsschritt element
	 */
	public void onelementAdded(@Observes @Added Zubereitungsschritt element) {
		zubereitungsschritteService.add(element);
	}
	
	/**
	 * Event zum l�schen eines Zubereitungsschrittes.
	 * @param Zubereitungsschritt element
	 */
	public void onelementDeleted(@Observes @Deleted Zubereitungsschritt element) {
		zubereitungsschritteService.delete(element);
	}

	/**
	 * Event zum aktualisieren eines Zubereitungsshcrittes
	 * @param Zubereitungsschritt element
	 */
	public void onelementUpdated(@Observes @Updated Zubereitungsschritt element) {
		zubereitungsschritteService.update(element);
	}

}
