package ch.gerberrenato.rezepte.data;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import ch.gerberrenato.rezepte.model.CodeTabelleTree;
import ch.gerberrenato.rezepte.services.CodeTabelleTreeService;
import ch.gerberrenato.rezepte.util.Events.Added;
import ch.gerberrenato.rezepte.util.Events.Deleted;
import ch.gerberrenato.rezepte.util.Events.Updated;

/**
 * Die Klasse stellt nur die Events f�r Codetabellen Tree (Parent-Child CodeTabelle) zurverf�gung.
 * @author Renato Gerber
 *
 */
@RequestScoped
public class CodeTableTreeProducer {
	
	@Inject
	private CodeTabelleTreeService codeTableTreeService;
	
	/**
	 * Hinzuf�gen einer Beziehung zwischen zwei CodeTabellen Elementen
	 * @param CodeTabelle codeTreeElement
	 */
	public void onAdded(@Observes @Added CodeTabelleTree codeTreeElement) {
		codeTableTreeService.add(codeTreeElement);
	}
	
	/**
	 * L�schen einer Beziehung zwischen zwei CodeTabellen Elementen
	 * @param CodeTabelle codeTreeElement
	 */
	public void onDeleted(@Observes @Deleted CodeTabelleTree codeTreeElement) {
		codeTableTreeService.delete(codeTreeElement);
	}
	
	/**
	 * Aktualisierung einer Beziehung zwischen zwei CodeTabellen Elementen
	 * @param CodeTabelle codeTreeElement
	 */
	public void onUpdated(@Observes @Updated CodeTabelleTree codeTreeElement) {
		codeTableTreeService.update(codeTreeElement);
	}

}
