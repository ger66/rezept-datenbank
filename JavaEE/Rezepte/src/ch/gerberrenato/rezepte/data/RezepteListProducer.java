package ch.gerberrenato.rezepte.data;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import ch.gerberrenato.rezepte.model.Rezept;
import ch.gerberrenato.rezepte.services.RezeptService;
import ch.gerberrenato.rezepte.util.Events.Added;
import ch.gerberrenato.rezepte.util.Events.Deleted;
import ch.gerberrenato.rezepte.util.Events.Updated;

/**
 * Klasse f�r alle Daten der Rezept �bersicht
 * @author Renato Gerber
 *
 */
@RequestScoped
public class RezepteListProducer {
	
	private List<Rezept> rezepte;

	@Inject
	private RezeptService rezeptService;

	@PostConstruct
	public void init() {
		this.rezepte = rezeptService.getAllRezepte();
	}
	
	/**
	 * Gibt ide Liste aller Rezepte aus
	 * @return List<Rezept>
	 */
	@Produces
	@Named
	public List<Rezept> getRezepte() {
		return this.rezepte;
	}
	
	/**
	 * Setzen einer Rezept Liste.
	 * @param List<Rezept> rezepte
	 */
	public void setRezepte(List<Rezept> rezepte) {
		this.rezepte = rezepte;
	}
	
	/**
	 * Event f�r das hinzuf�gen eines Rezeptes.
	 * @param Rezept rezept
	 */
	public void onRezeptAdded(@Observes @Added Rezept rezept) {
		rezeptService.add(rezept);
		init();
	}
	
	/**
	 * Event zum l�schen eines Rezeptes
	 * @param Rezept rezept
	 */
	public void onRezeptDeleted(@Observes @Deleted Rezept rezept) {
		rezeptService.delete(rezept);
		init();
	}
	
	/**
	 * Event zum aktualisieren eines Rezepts
	 * @param Rezept rezept
	 */
	public void onRezeptUpdated(@Observes @Updated Rezept rezept) {
		rezeptService.update(rezept);
		init();
	}
}
