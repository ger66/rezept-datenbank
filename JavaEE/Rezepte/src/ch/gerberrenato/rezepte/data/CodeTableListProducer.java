package ch.gerberrenato.rezepte.data;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import ch.gerberrenato.rezepte.model.CodeTabelle;
import ch.gerberrenato.rezepte.services.CodeTabeleService;
import ch.gerberrenato.rezepte.util.Events.Added;
import ch.gerberrenato.rezepte.util.Events.Deleted;
import ch.gerberrenato.rezepte.util.Events.Updated;
/**
 * Dies Klasse stellt die Codetabellen Daten bereit.
 * @author Renato
 *
 */
@RequestScoped
public class CodeTableListProducer {
	
	private List<CodeTabelle> codeTabelle;
	private List<CodeTabelle> codeTabelleChilds;
	
	@Inject
	private CodeTabeleService codeTableService;

	@PostConstruct
	public void init() {
		codeTabelle = codeTableService.getAllCodes();
		codeTabelleChilds = codeTableService.getAllChildCodes();
	}
	
	/**
	 * Diese Methode gibt die Liste alles CodeTablles aus
	 * @return List<CodeTabelle>
	 */
	@Produces
	@Named
	public List<CodeTabelle> getCodeTabelle() {
		return codeTabelle;
	}
	
	/**
	 * Diese Methode Listet alle Kind Elemente zu einem Code-Element auf.
	 * @return List<CodeTabelle>
	 */
	@Produces
	@Named
	public List<CodeTabelle> getCodeTabelleChilds() {
		return codeTabelleChilds;
	}
	
	/**
	 * Dies Methode git alle Einheiten aus (Teilmenge von CodeTabelle)
	 * @return List<CodeTabelle>
	 */
	@Produces
	@Named
	public List<CodeTabelle> getCodeGruppenListEinheit() {
		return codeTableService.getGruppe("EINHEITEN");
	}
	
	/**
	 * Dies Methode git alle TCM Elemente aus (Teilmenge von CodeTabelle)
	 * @return List<CodeTabelle>
	 */
	@Produces
	@Named
	public List<CodeTabelle> getCodeGruppenListTcm() {
		return codeTableService.getGruppe("TCM");
	}
	
	/**
	 * Dies Methode git alle PRdoukte Kategortie aus (Teilmenge von CodeTabelle)
	 * @return List<CodeTabelle>
	 */
	@Produces
	@Named
	public List<CodeTabelle> getCodeGruppenListProdKat() {
		return codeTableService.getGruppe("PRODKAT");
	}
	
	/**
	 * Dies Methode git alle Men� Kategorien aus (Teilmenge von CodeTabelle)
	 * @return List<CodeTabelle>
	 */
	@Produces
	@Named
	public List<CodeTabelle> getCodeGruppenListMenueKat() {
		return codeTableService.getGruppe("MENUEKAT");
	}
	
	/**
	 * Event Code-Element hizuf�gen
	 * @param CodeTabelle codeElement
	 */
	public void onCodeElementAdded(@Observes @Added CodeTabelle codeElement) {
		codeTableService.add(codeElement);
		init();
	}
	
	/**
	 * Event Code-Element l�schen
	 * @param CodeTabelle codeElement
	 */
	public void onCodeElementDeleted(@Observes @Deleted CodeTabelle codeElement) {
		codeTableService.delete(codeElement);
		init();
	}

	/**
	 * Event Aktualisieren Code-Element
	 * @param CodeTabelle codeElement
	 */
	public void onCodeElementUpdated(@Observes @Updated CodeTabelle codeElement) {
		codeTableService.update(codeElement);
		init();
	}
}
