package ch.gerberrenato.rezepte.data;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import ch.gerberrenato.rezepte.model.Zutat;
import ch.gerberrenato.rezepte.services.ZutatenService;
import ch.gerberrenato.rezepte.util.Events.Added;
import ch.gerberrenato.rezepte.util.Events.Deleted;
import ch.gerberrenato.rezepte.util.Events.Updated;

/**
 * Klasse f�r die manipulation der Zutaten
 * @author Renato Gerber
 *
 */
@RequestScoped
public class ZutatenListProducer {
	
	@Inject
	private ZutatenService zutatenService;

	/**
	 * Event f�r das hinzuf�gen einer Zutat 
	 * @param Zutat element
	 */
	public void onelementAdded(@Observes @Added Zutat element) {
		zutatenService.add(element);
	}
	
	/**
	 * Event f�r das l�schen einer Zutat
	 * @param Zutat element
	 */
	public void onelementDeleted(@Observes @Deleted Zutat element) {
		zutatenService.delete(element);
	}
	
	/**
	 * Event zum aktualisieren einer Zutat
	 * @param Zutat element
	 */
	public void onelementUpdated(@Observes @Updated Zutat element) {
		zutatenService.update(element);
	}

}
