package ch.gerberrenato.rezepte.model;

import java.io.Serializable;

/**
 * Komplexer Datentyp f�r die Produkte
 * @author Renato Gerber
 *
 */
public class Produkt implements Serializable{
	private static final long serialVersionUID = 1L;

	private long produktNr;
	private String name;
	private String beschreibung;
	private double gewicht;
	private int einheit;
	private int tcmElement;
	private int kategorie;
	private int aktiv_f;

	/**
	 * Konstruktor ohne Parameter
	 */
	public Produkt() { }
	
	/**
	 * Konstruktor mit allen Parameter
	 * @param produkteNr
	 * @param name
	 * @param beschreibung
	 * @param gewicht
	 * @param einheit
	 * @param tcmElement
	 * @param kategorie
	 * @param aktiv_f
	 */
	public Produkt(long produkteNr, String name, String beschreibung, double gewicht, int einheit, int tcmElement, int kategorie, int aktiv_f){
		this.produktNr = produkteNr;
		this.name = name;
		this.beschreibung = beschreibung;
		this.gewicht = gewicht;
		this.einheit = einheit;
		this.tcmElement = tcmElement;
		this.kategorie = kategorie;
		this.aktiv_f = aktiv_f;
	}
	
	public long getProduktNr() {
		return produktNr;
	}

	public void setProduktNr(long produktNr) {
		this.produktNr = produktNr;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}

	public double getGewicht() {
		return gewicht;
	}

	public void setGewicht(double gewicht) {
		this.gewicht = gewicht;
	}

	public int getEinheit() {
		return einheit;
	}

	public void setEinheit(int einheit) {
		this.einheit = einheit;
	}

	public int getTcmElement() {
		return tcmElement;
	}

	public void setTcmElement(int tcmElement) {
		this.tcmElement = tcmElement;
	}

	public int getKategorie() {
		return kategorie;
	}

	public void setKategorie(int kategorie) {
		this.kategorie = kategorie;
	}

	public int getAktiv_f() {
		return aktiv_f;
	}

	public void setAktiv_f(int aktiv_f) {
		this.aktiv_f = aktiv_f;
	}
	
}
