package ch.gerberrenato.rezepte.model;

import java.io.Serializable;

/**
 * Komplexer Datentyp f�r die Zutaten
 * @author Renato Gerber
 *
 */
public class Zutat implements Serializable{
	private static final long serialVersionUID = 1L;

	private long zutatenNr;
	private long rezeptNr;
	private long produktNr;
	private String produktBez;
	private double menge;
	private long einheitNr;
	private String einheitBez;
	
	/**
	 * Konstruktor ohne Parameter
	 */
	public Zutat(){}

	
	/**
	 * Konstruktor mit allen Parametern
	 * @param zutatenNr
	 * @param rezeptNr
	 * @param produktNr
	 * @param menge
	 * @param einheitNr
	 */
	public Zutat(long zutatenNr, long rezeptNr, long produktNr, double menge, long einheitNr) {
		super();
		this.zutatenNr = zutatenNr;
		this.rezeptNr = rezeptNr;
		this.produktNr = produktNr;
		this.menge = menge;
		this.einheitNr = einheitNr;
	}

	public long getZutatenNr() {
		return zutatenNr;
	}

	public void setZutatenNr(long zutatenNr) {
		this.zutatenNr = zutatenNr;
	}

	public long getRezeptNr() {
		return rezeptNr;
	}

	public void setRezeptNr(long rezeptNr) {
		this.rezeptNr = rezeptNr;
	}

	public long getProduktNr() {
		return produktNr;
	}

	public void setProduktNr(long produktNr) {
		this.produktNr = produktNr;
	}

	public double getMenge() {
		return menge;
	}

	public void setMenge(double menge) {
		this.menge = menge;
	}

	public long getEinheitNr() {
		return einheitNr;
	}

	public void setEinheitNr(long einheitNr) {
		this.einheitNr = einheitNr;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public String getProduktBez() {
		return produktBez;
	}

	public void setProduktBez(String produktBez) {
		this.produktBez = produktBez;
	}

	public String getEinheitBez() {
		return einheitBez;
	}

	public void setEinheitBez(String einheitBez) {
		this.einheitBez = einheitBez;
	}
		
}
