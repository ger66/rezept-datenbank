package ch.gerberrenato.rezepte.model;

import java.io.Serializable;

/**
 * Komplexer Datentyp f�r die Betiehung Parent-Child in den CodeTabellen
 * @author Renato
 *
 */
public class CodeTabelleTree implements Serializable {
	private static final long serialVersionUID = 1L;

	private long codeTabellenTreeId;
	private long parent;
	private long child;
	
	/**
	 * Konstruktor ohne Parameter
	 */
	public CodeTabelleTree(){}
	
	/**
	 * Konstruktor mit allen Parameter
	 * @param codeTabellenTreeId
	 * @param parent
	 * @param child
	 */
	public CodeTabelleTree(long codeTabellenTreeId, long parent, long child){
		this.codeTabellenTreeId = codeTabellenTreeId;
		this.parent = parent;
		this.child = child;
	}

	public long getCodeTabellenTreeId() {
		return codeTabellenTreeId;
	}
	public void setCodeTabellenTreeId(int codeTabellenTreeId) {
		this.codeTabellenTreeId = codeTabellenTreeId;
	}
	public long getParent() {
		return parent;
	}
	public void setParent(int parent) {
		this.parent = parent;
	}
	public long getChild() {
		return child;
	}
	public void setChild(long newCodeElement) {
		this.child = newCodeElement;
	}
	

}

