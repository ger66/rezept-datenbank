package ch.gerberrenato.rezepte.model;

import java.io.Serializable;
import java.util.List;

/**
 * Komplexer Daten Typ CodeTabelle
 * @author Renato Gerber
 *
 */
public class CodeTabelle implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private long codeTabelleNr;
	private String bezeichnung;
	private String kurzBezeichnung;
	private int aktiv_f;
	private int ownerNr;
	private int isParent;
	private List<CodeTabelle> childs;
	private long codeTabelleTreeId;

	/**
	 * Konstruktor ohne Parameter
	 */
	public CodeTabelle(){}
	
	/**
	 * Konstuktor mit allen Attributen
	 * @param codeTabelleNr
	 * @param bezeichnung
	 * @param kurzBezeichnung
	 * @param aktiv_f
	 * @param ownerNr
	 * @param isParent
	 * @param childCodes
	 */
	public CodeTabelle(long codeTabelleNr, String bezeichnung, String kurzBezeichnung, int aktiv_f, int ownerNr, int isParent, List<CodeTabelle> childCodes){
		this(codeTabelleNr, bezeichnung, kurzBezeichnung, aktiv_f, ownerNr, isParent, childCodes, 0L);
	}

	/**
	 * 
	 * @param codeTabelleNr
	 * @param bezeichnung
	 * @param kurzBezeichnung
	 * @param aktiv_f
	 * @param ownerNr
	 * @param isParent
	 */
	public CodeTabelle(long codeTabelleNr, String bezeichnung, String kurzBezeichnung, int aktiv_f, int ownerNr, int isParent, List<CodeTabelle> childCodes, long codeTabelleTreeId){
		
		this.codeTabelleNr = codeTabelleNr;
		this.bezeichnung = bezeichnung;
		this.kurzBezeichnung = kurzBezeichnung;
		this.aktiv_f = aktiv_f;
		this.ownerNr = ownerNr;
		this.isParent = isParent;
		this.childs = childCodes;
		this.codeTabelleTreeId = codeTabelleTreeId;
	}
	
	
	public long getCodeTabelleNr() {
		return codeTabelleNr;
	}
	public void setCodeTabelleNr(long codeTabelleNr) {
		this.codeTabelleNr = codeTabelleNr;
	}
	public String getBezeichnung() {
		return bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	public String getKurzBezeichnung() {
		return kurzBezeichnung;
	}
	public void setKurzBezeichnung(String kurzBezeichnung) {
		this.kurzBezeichnung = kurzBezeichnung;
	}
	public int getAktiv_f() {
		return aktiv_f;
	}
	public void setAktiv_f(int aktiv_f) {
		this.aktiv_f = aktiv_f;
	}
	public int getOwnerNr() {
		return ownerNr;
	}
	public void setOwnerNr(int ownerNr) {
		this.ownerNr = ownerNr;
	}
	public int getIsParent() {
		return isParent;
	}
	public void setIsParent(int isParent) {
		this.isParent = isParent;
	}
	public List<CodeTabelle> getChildCodes() {
		return childs;
	}
	public void setChildCodes(List<CodeTabelle> childCodes) {
		this.childs  = childCodes;
	}
	public long getCodeTabelleTreeId() {
		return codeTabelleTreeId;
	}
	public void setCodeTabelleTreeID(long codeTabelleTreeId) {
		this.codeTabelleTreeId = codeTabelleTreeId;
	}
	
}