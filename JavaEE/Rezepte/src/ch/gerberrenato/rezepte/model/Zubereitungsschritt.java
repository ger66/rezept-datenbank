package ch.gerberrenato.rezepte.model;

/**
 * Komplexer Datentyp f�r die Zubereitungsschritte
 * @author Renato Gerber
 *
 */
public class Zubereitungsschritt {
	
	long zubereitungsschrittNr;
    long rezeptNr;
    long reihenfolgeNr;
    String beschreibung;


	/**
     * Konstruktor ohne Parameter
     */
    public Zubereitungsschritt() {}

	/**
	 * Konstruktor mit allen Parametern
	 * @param zubereitungsschrittNr
	 * @param rezeptNr
	 * @param reihenfolgeNr
	 * @param beschreibung
	 */
	public Zubereitungsschritt(long zubereitungsschrittNr, long rezeptNr, long reihenfolgeNr, String beschreibung) {
		super();
		this.zubereitungsschrittNr = zubereitungsschrittNr;
		this.rezeptNr = rezeptNr;
		this.reihenfolgeNr = reihenfolgeNr;
		this.beschreibung = beschreibung;
	}
	
	
	public long getZubereitungsschrittNr() {
		return zubereitungsschrittNr;
	}
	public void setZubereitungsschrittNr(long zubereitungsschrittNr) {
		this.zubereitungsschrittNr = zubereitungsschrittNr;
	}
	public long getRezeptNr() {
		return rezeptNr;
	}
	public void setRezeptNr(long rezeptNr) {
		this.rezeptNr = rezeptNr;
	}
	public long getReihenfolgeNr() {
		return reihenfolgeNr;
	}
	public void setReihenfolgeNr(long reihenfolgeNr) {
		this.reihenfolgeNr = reihenfolgeNr;
	}
	public String getBeschreibung() {
		return beschreibung;
	}
	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}

}
