package ch.gerberrenato.rezepte.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Komplexer Datentyp Rezept
 * @author Renato Gerber
 *
 */
public class Rezept implements Serializable{
	private static final long serialVersionUID = 1L;

	private long rezeptNr;
	private String name;
	private String beschreibung;
	private int kalorien;
	private String kalorienBez;
	private int kategorieNr;
	private String kategorieBez;
	private int aktiv_f;
	private int ownerNr;
	private Date gueltigAb;
	private Date gueltigBis;

	/**
	 * Konstruktor ohne Parameter
	 */
	public Rezept() { }
	
	/**
	 * Konstruktor mit allen Parametern
	 * @param rezeptNr
	 * @param name
	 * @param beschreibung
	 * @param kalorien
	 * @param kategorieNr
	 * @param aktiv
	 * @param ownerNr
	 * @param gueltigAb
	 * @param gueltigBis
	 */
	public Rezept(long rezeptNr, String name, String beschreibung, int kalorien, int kategorieNr, int aktiv, int ownerNr, Date gueltigAb, Date gueltigBis) {
		this.rezeptNr = rezeptNr;
		this.name = name;
		this.beschreibung = beschreibung;
		this.kalorien = kalorien;
		this.kategorieNr = kategorieNr;
		this.aktiv_f = aktiv;
		this.ownerNr = ownerNr;
		this.gueltigAb = gueltigAb;
		this.gueltigBis = gueltigBis; 
	}
	
	public long getRezeptNr() {
		return rezeptNr;
	}

	public void setRezeptNr(long rezeptNr) {
		this.rezeptNr = rezeptNr;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getBeschreibung() {
		return beschreibung;
	}
	
	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}
	
	public int getKalorien() {
		return kalorien;
	}
	
	public void setKalorien(int kalorien) {
		this.kalorien = kalorien;
	}
	
	public int getKategorieNr() {
		return kategorieNr;
	}
	
	public void setKategorieNr(int kategorieNr) {
		this.kategorieNr = kategorieNr;
	}

	public int getOwnerNr() {
		return ownerNr;
	}
	
	public void setOwnerNr(int ownerNr) {
		this.ownerNr = ownerNr;
	}

	public int getAktiv_f() {
		return aktiv_f;
	}
	public void setAktiv_f(int aktiv_f) {
		this.aktiv_f = aktiv_f;
	}

	public Date getGueltigAb() {
		return gueltigAb;
	}

	public void setGueltigAb(Date gueltigAb) {
		this.gueltigAb = gueltigAb;
	}

	public Date getGueltigBis() {
		return gueltigBis;
	}

	public void setGueltigBis(Date gueltigBis) {
		this.gueltigBis = gueltigBis;
	}

	public String getKalorienBez() {
		return kalorienBez;
	}

	public void setKalorienBez(String kalorienBez) {
		this.kalorienBez = kalorienBez;
	}

	public String getKategorieBez() {
		return kategorieBez;
	}

	public void setKategorieBez(String kategorieBez) {
		this.kategorieBez = kategorieBez;
	}

	
	
	
}
