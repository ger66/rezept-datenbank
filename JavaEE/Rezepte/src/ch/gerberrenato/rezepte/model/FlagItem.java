package ch.gerberrenato.rezepte.model;

import java.io.Serializable;


public class FlagItem implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public String bezeichnung;
	public int value;
	
	public FlagItem(){}
	
	public FlagItem(String bezeichnung, int value) {
		this.bezeichnung = bezeichnung;
		this.value = value;
	}
	
	public String getBezeichnung() {
		return bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	
}
