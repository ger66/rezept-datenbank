
truncate table `rezeptedatenbank`.`codetabelle`;
truncate table `rezeptedatenbank`.`codetabelletree`;

INSERT INTO `rezeptedatenbank`.`codetabelle`
(`Bezeichnung`,
`KurzBezeichnung`,
`ownerNr`,
`aktiv_f`,
`IsParent`)
VALUES
('Menü Kategorien', 'Menü Kat.', 1, 1, 1),
('Frühling Menü', 'Frühling', 1, 1, 2),
('Sommer Menü', 'Sommer', 1, 1, 2),
('Herbst Menü', 'Herbst', 1, 1, 2),
('Winter Menü', 'Winter', 1, 1, 2);


SELECT `codetabelle`.`CodetabelleNr`,
    `codetabelle`.`Bezeichnung`,
    `codetabelle`.`KurzBezeichnung`,
    `codetabelle`.`ownerNr`,
    `codetabelle`.`aktiv_f`,
    `codetabelle`.`IsParent`
FROM `rezeptedatenbank`.`codetabelle`;



INSERT INTO `rezeptedatenbank`.`codetabelletree`
(`Parent`,
`Child`)
VALUES
(1, 2),
(1, 3),
(1, 4),
(1, 5);

SELECT `codetabelletree`.`Codetabellentree_Id`,
    `codetabelletree`.`Parent`,
    `codetabelletree`.`Child`
FROM `rezeptedatenbank`.`codetabelletree`;



