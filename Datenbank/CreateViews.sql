
CREATE VIEW `ProduktKategorie` AS
(
SELECT code.*, 0 as Codetabellentree_Id 
FROM rezepteDatenbank.codetabelle code 
INNER JOIN rezepteDatenbank.codetabelletree tree 
	ON tree.child = code.CodetabelleNr 
INNER JOIN rezepteDatenbank.codetabelle codeParent 
	ON codeParent.CodetabelleNr = tree.parent 
	AND codeParent.KurzBezeichnung = 'PRODKAT'
WHERE code.aktiv_f = 1
);
select * from produktkategorie;


CREATE VIEW `menuekategorien` AS
(
SELECT code.*, 0 as Codetabellentree_Id 
FROM rezepteDatenbank.codetabelle code 
INNER JOIN rezepteDatenbank.codetabelletree tree 
	ON tree.child = code.CodetabelleNr 
INNER JOIN rezepteDatenbank.codetabelle codeParent 
	ON codeParent.CodetabelleNr = tree.parent 
	AND codeParent.KurzBezeichnung = 'REZKAT'
WHERE code.aktiv_f = 1
);
select * from menuekategorien;


CREATE VIEW `tcmelemente` AS
(
SELECT code.*, 0 as Codetabellentree_Id 
FROM rezepteDatenbank.codetabelle code 
INNER JOIN rezepteDatenbank.codetabelletree tree 
	ON tree.child = code.CodetabelleNr 
INNER JOIN rezepteDatenbank.codetabelle codeParent 
	ON codeParent.CodetabelleNr = tree.parent 
	AND codeParent.KurzBezeichnung = 'TCM'
WHERE code.aktiv_f = 1
);
select * from tcmelemente;


CREATE VIEW `einheiten` AS
(
SELECT code.*, 0 as Codetabellentree_Id 
FROM rezepteDatenbank.codetabelle code 
INNER JOIN rezepteDatenbank.codetabelletree tree 
	ON tree.child = code.CodetabelleNr 
INNER JOIN rezepteDatenbank.codetabelle codeParent 
	ON codeParent.CodetabelleNr = tree.parent 
	AND codeParent.KurzBezeichnung = 'EINHEITEN'
WHERE code.aktiv_f = 1
);
select * from einheiten;