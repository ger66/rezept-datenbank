-- MySQL dump 10.13  Distrib 5.6.29, for Win64 (x86_64)
--
-- Host: localhost    Database: rezeptedatenbank
-- ------------------------------------------------------
-- Server version	5.6.29-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `benutzer`
--

DROP TABLE IF EXISTS `benutzer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `benutzer` (
  `BenutzerNr` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(250) NOT NULL,
  `Vorname` varchar(150) NOT NULL,
  `MailAdresse` varchar(150) DEFAULT NULL,
  `Passwort` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`BenutzerNr`),
  UNIQUE KEY `BenutzerNr_UNIQUE` (`BenutzerNr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bilder`
--

DROP TABLE IF EXISTS `bilder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bilder` (
  `BilderNr` int(11) NOT NULL AUTO_INCREMENT,
  `BildName` varchar(150) DEFAULT NULL,
  `BlobBild` mediumblob NOT NULL,
  `RezeptNr` int(11) DEFAULT NULL,
  PRIMARY KEY (`BilderNr`),
  UNIQUE KEY `BilderNr_UNIQUE` (`BilderNr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `codetabelle`
--

DROP TABLE IF EXISTS `codetabelle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `codetabelle` (
  `CodetabelleNr` int(11) NOT NULL AUTO_INCREMENT,
  `Bezeichnung` varchar(250) NOT NULL,
  `KurzBezeichnung` varchar(80) DEFAULT NULL,
  `ownerNr` int(11) NOT NULL,
  `aktiv_f` int(11) DEFAULT NULL,
  `IsParent` int(4) DEFAULT NULL,
  PRIMARY KEY (`CodetabelleNr`),
  UNIQUE KEY `RolleNr_UNIQUE` (`CodetabelleNr`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `codetabelletree`
--

DROP TABLE IF EXISTS `codetabelletree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `codetabelletree` (
  `Codetabellentree_Id` int(11) NOT NULL AUTO_INCREMENT,
  `Parent` int(11) NOT NULL,
  `Child` int(11) NOT NULL,
  PRIMARY KEY (`Codetabellentree_Id`),
  UNIQUE KEY `CodtabelleGruppe_Id_UNIQUE` (`Codetabellentree_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Temporary view structure for view `einheiten`
--

DROP TABLE IF EXISTS `einheiten`;
/*!50001 DROP VIEW IF EXISTS `einheiten`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `einheiten` AS SELECT 
 1 AS `CodetabelleNr`,
 1 AS `Bezeichnung`,
 1 AS `KurzBezeichnung`,
 1 AS `ownerNr`,
 1 AS `aktiv_f`,
 1 AS `IsParent`,
 1 AS `Codetabellentree_Id`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `einheitentransformer`
--

DROP TABLE IF EXISTS `einheitentransformer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `einheitentransformer` (
  `EinheitTransformerNr` int(11) NOT NULL AUTO_INCREMENT,
  `BasisEinheit` int(11) NOT NULL,
  `ZielEinheit` int(11) NOT NULL,
  `Faktor` decimal(16,8) NOT NULL,
  PRIMARY KEY (`EinheitTransformerNr`),
  UNIQUE KEY `CodetabelleTypenNr_UNIQUE` (`EinheitTransformerNr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Temporary view structure for view `menuekategorien`
--

DROP TABLE IF EXISTS `menuekategorien`;
/*!50001 DROP VIEW IF EXISTS `menuekategorien`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `menuekategorien` AS SELECT 
 1 AS `CodetabelleNr`,
 1 AS `Bezeichnung`,
 1 AS `KurzBezeichnung`,
 1 AS `ownerNr`,
 1 AS `aktiv_f`,
 1 AS `IsParent`,
 1 AS `Codetabellentree_Id`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `produkte`
--

DROP TABLE IF EXISTS `produkte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produkte` (
  `ProdukteNr` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(250) NOT NULL,
  `Beschreibung` varchar(5000) DEFAULT NULL,
  `Gewicht` int(11) DEFAULT NULL,
  `Einheit` int(11) DEFAULT NULL,
  `TcmElement` int(11) DEFAULT NULL,
  `Kategorie` int(11) DEFAULT NULL,
  `aktiv_f` int(4) DEFAULT NULL,
  PRIMARY KEY (`ProdukteNr`),
  UNIQUE KEY `ProdukteNr_UNIQUE` (`ProdukteNr`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Temporary view structure for view `produktkategorie`
--

DROP TABLE IF EXISTS `produktkategorie`;
/*!50001 DROP VIEW IF EXISTS `produktkategorie`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `produktkategorie` AS SELECT 
 1 AS `CodetabelleNr`,
 1 AS `Bezeichnung`,
 1 AS `KurzBezeichnung`,
 1 AS `ownerNr`,
 1 AS `aktiv_f`,
 1 AS `IsParent`,
 1 AS `Codetabellentree_Id`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `rezepte`
--

DROP TABLE IF EXISTS `rezepte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rezepte` (
  `RezeptNr` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(350) NOT NULL,
  `Beschreibung` varchar(4000) DEFAULT NULL,
  `ownerNr` int(11) DEFAULT NULL,
  `GueltigAb` datetime DEFAULT NULL,
  `GueltigBis` datetime DEFAULT NULL,
  `kalorien` int(11) DEFAULT NULL,
  `kategorieNr` int(11) DEFAULT NULL,
  `aktiv_f` int(1) DEFAULT NULL,
  PRIMARY KEY (`RezeptNr`),
  UNIQUE KEY `RezeptNr_UNIQUE` (`RezeptNr`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Temporary view structure for view `tcmelemente`
--

DROP TABLE IF EXISTS `tcmelemente`;
/*!50001 DROP VIEW IF EXISTS `tcmelemente`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `tcmelemente` AS SELECT 
 1 AS `CodetabelleNr`,
 1 AS `Bezeichnung`,
 1 AS `KurzBezeichnung`,
 1 AS `ownerNr`,
 1 AS `aktiv_f`,
 1 AS `IsParent`,
 1 AS `Codetabellentree_Id`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `zubereitungsschritte`
--

DROP TABLE IF EXISTS `zubereitungsschritte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zubereitungsschritte` (
  `ZubereitungsschrittNr` int(11) NOT NULL AUTO_INCREMENT,
  `RezeptNr` int(11) NOT NULL,
  `ReihenfolgeNr` int(11) NOT NULL,
  `Beschreibung` varchar(15000) DEFAULT NULL,
  PRIMARY KEY (`ZubereitungsschrittNr`),
  UNIQUE KEY `ZubereitungsschrittNr_UNIQUE` (`ZubereitungsschrittNr`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zutaten`
--

DROP TABLE IF EXISTS `zutaten`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zutaten` (
  `ZutatenNr` int(11) NOT NULL AUTO_INCREMENT,
  `RezeptNr` int(11) NOT NULL,
  `ProduktNr` int(11) NOT NULL,
  `Menge` double DEFAULT NULL,
  `EinheitNr` int(11) DEFAULT NULL,
  PRIMARY KEY (`ZutatenNr`),
  UNIQUE KEY `idZutaten_UNIQUE` (`ZutatenNr`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Final view structure for view `einheiten`
--

/*!50001 DROP VIEW IF EXISTS `einheiten`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `einheiten` AS (select `code`.`CodetabelleNr` AS `CodetabelleNr`,`code`.`Bezeichnung` AS `Bezeichnung`,`code`.`KurzBezeichnung` AS `KurzBezeichnung`,`code`.`ownerNr` AS `ownerNr`,`code`.`aktiv_f` AS `aktiv_f`,`code`.`IsParent` AS `IsParent`,0 AS `Codetabellentree_Id` from ((`codetabelle` `code` join `codetabelletree` `tree` on((`tree`.`Child` = `code`.`CodetabelleNr`))) join `codetabelle` `codeparent` on(((`codeparent`.`CodetabelleNr` = `tree`.`Parent`) and (`codeparent`.`KurzBezeichnung` = 'EINHEITEN')))) where (`code`.`aktiv_f` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `menuekategorien`
--

/*!50001 DROP VIEW IF EXISTS `menuekategorien`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `menuekategorien` AS (select `code`.`CodetabelleNr` AS `CodetabelleNr`,`code`.`Bezeichnung` AS `Bezeichnung`,`code`.`KurzBezeichnung` AS `KurzBezeichnung`,`code`.`ownerNr` AS `ownerNr`,`code`.`aktiv_f` AS `aktiv_f`,`code`.`IsParent` AS `IsParent`,0 AS `Codetabellentree_Id` from ((`codetabelle` `code` join `codetabelletree` `tree` on((`tree`.`Child` = `code`.`CodetabelleNr`))) join `codetabelle` `codeparent` on(((`codeparent`.`CodetabelleNr` = `tree`.`Parent`) and (`codeparent`.`KurzBezeichnung` = 'REZKAT')))) where (`code`.`aktiv_f` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `produktkategorie`
--

/*!50001 DROP VIEW IF EXISTS `produktkategorie`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `produktkategorie` AS (select `code`.`CodetabelleNr` AS `CodetabelleNr`,`code`.`Bezeichnung` AS `Bezeichnung`,`code`.`KurzBezeichnung` AS `KurzBezeichnung`,`code`.`ownerNr` AS `ownerNr`,`code`.`aktiv_f` AS `aktiv_f`,`code`.`IsParent` AS `IsParent`,0 AS `Codetabellentree_Id` from ((`codetabelle` `code` join `codetabelletree` `tree` on((`tree`.`Child` = `code`.`CodetabelleNr`))) join `codetabelle` `codeparent` on(((`codeparent`.`CodetabelleNr` = `tree`.`Parent`) and (`codeparent`.`KurzBezeichnung` = 'PRODKAT')))) where (`code`.`aktiv_f` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `tcmelemente`
--

/*!50001 DROP VIEW IF EXISTS `tcmelemente`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `tcmelemente` AS (select `code`.`CodetabelleNr` AS `CodetabelleNr`,`code`.`Bezeichnung` AS `Bezeichnung`,`code`.`KurzBezeichnung` AS `KurzBezeichnung`,`code`.`ownerNr` AS `ownerNr`,`code`.`aktiv_f` AS `aktiv_f`,`code`.`IsParent` AS `IsParent`,0 AS `Codetabellentree_Id` from ((`codetabelle` `code` join `codetabelletree` `tree` on((`tree`.`Child` = `code`.`CodetabelleNr`))) join `codetabelle` `codeparent` on(((`codeparent`.`CodetabelleNr` = `tree`.`Parent`) and (`codeparent`.`KurzBezeichnung` = 'TCM')))) where (`code`.`aktiv_f` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-29 20:58:33
